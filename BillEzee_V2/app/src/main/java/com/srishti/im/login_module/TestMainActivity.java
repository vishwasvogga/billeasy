package com.srishti.im.login_module;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.srishti.be.loginoperations.fragments.BELoginMainPageFragment;
import com.srishti.be.loginoperations.fragments.BELoginRegisterNewUserFragment;
import com.srishti.be.utility.SRFragmentTransactionHelper;

import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ROLE_KEY;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_MAIN_FRAGMENT_TAG;
import static com.srishti.be.utility.constants.SRApplicationConstants.srFragmentBridge;

public class TestMainActivity extends AppCompatActivity {
    private FragmentManager fragmentManager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_test_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final SRFragmentTransactionHelper srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(this);
        srFragmentTransactionHelper.ReplaceFragment(new BELoginMainPageFragment(),SR_LOGIN_MAIN_FRAGMENT_TAG,R.id.FL_mainActFrame,SR_LOGIN_MAIN_FRAGMENT_TAG);
       //Add user floating action button
        FloatingActionButton addUser = (FloatingActionButton) findViewById(R.id.FAB_Add);
        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BELoginRegisterNewUserFragment addUserFragment = new BELoginRegisterNewUserFragment();
                Bundle configIn = new Bundle();
                configIn.putInt(USER_ROLE_KEY, 1);  //Super admin creating admin
                addUserFragment.setArguments(configIn);
                srFragmentTransactionHelper.ReplaceFragment(addUserFragment,"ADDUSER",R.id.FL_mainActFrame,"ADDUSER");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //static variable from application constants
        if(srFragmentBridge !=null){
            srFragmentBridge.onBackPressed(null,null);
        }else{
            super.onBackPressed();
        }
    }
}
