package com.srishti.im.login_module;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.srishti.be.utility.SRFragmentTransactionHelper;


/**
 * DOG Fragment
 */

public class fragmentDog extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return getActivity().getLayoutInflater().inflate(R.layout.fragment,null);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("FRAGMNETS","number of frag in backstack "+ SRFragmentTransactionHelper.getInstance(getActivity()).numberOfFragmentBackstack());
        Button back = (Button) getActivity().findViewById(R.id.B_F_button1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back
                SRFragmentTransactionHelper.getInstance(getActivity()).popBackStack();
            }
        });

        Button next = (Button) getActivity().findViewById(R.id.B_F_button2);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go next
            }
        });

    }
}
