package com.srishti.be.utility;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * This class provides methods to close/hide the keypaid
 */

public class SRCloseKeyboard {
    /**
     * This method is used to close the open keyboard
     * @param context
     */
    public void closeTheKeyBoard(final Context context){
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Check if no view has focus:
                View view = ((Activity)context).getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)((Activity)context).getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
    }
}
