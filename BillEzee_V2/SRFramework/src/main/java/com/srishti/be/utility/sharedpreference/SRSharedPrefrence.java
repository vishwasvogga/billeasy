package com.srishti.be.utility.sharedpreference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SHARED_PREFRENCE_KEY;

/**
 * This class provides methods for shared preference
 */

public class SRSharedPrefrence {
    private static SRSharedPrefrence srSharedPrefrence=null;
    private Context context=null;
    private SharedPreferences sharedPreferences=null;

    /**
     * private constructor
     * @param context context
     */
    public SRSharedPrefrence(Context context) {
        this.context = context;
        getSharedPrefrence();
    }

    /**
     * This method is used to get the singleton instance of this class
     * @param context context
     * @return singleton instance of this class
     */
    public static SRSharedPrefrence getInstance(Context context){
        if(srSharedPrefrence==null){
            srSharedPrefrence = new SRSharedPrefrence(context);
        }
        return srSharedPrefrence;
    }

    /**
     * This method is used to clear the previously created singleton instance
     */
    public static void clearInstance(){
        if(srSharedPrefrence!=null){
            srSharedPrefrence=null;
        }
    }

    /**
     * This method returns the shared preference of this appliaction
     */
    private void getSharedPrefrence(){
        SharedPreferences sharedpreferences = ((Activity)context).
                getSharedPreferences(SR_SHARED_PREFRENCE_KEY, Context.MODE_PRIVATE);
        this.sharedPreferences = sharedpreferences;
    }

    /**
     * This metod is used to put string shared preference
     * @param key key of the value
     * @param value string value
     */
    public void putString(String key,String value){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.apply();
        editor.commit();
    }

    /**
     * This metod is used to put integer shared preference
     * @param key key of the value
     * @param value string value
     */
    public void putInt(String key,int value){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key,value);
        editor.apply();
        editor.commit();
    }

    /**
     * This method is used to get the string from shared preference
     * @param key key of the value
     * @param defaultValue if the entity not present retrun this
     * @return value of the key
     */
    public String getString(String key,String defaultValue){
        return sharedPreferences.getString(key,defaultValue);
    }

    /**
     * This method is used to get the Integer from shared preference
     * @param key key of the value
     * @param defaultValue if the entity not present retrun this
     * @return value of the key
     */
    public int getInt(String key,int defaultValue){
        return sharedPreferences.getInt(key,defaultValue);
    }




}
