package com.srishti.be.utility.interfaces;

/**
 * THis interface is used to bridge the framework activity and the child activitys
 */

public interface SRActivityBridge {
    /**
     * This method is used to bridge between activity and the fragment
     * @param object1 optional argument 1
     * @param object2 optional argument 2
     */
    public void function1(Object object1, Object object2);

    /**
     * This method is used to bridge between activity and the fragment
     * @param object1 optional argument 1
     * @param object2 optional argument 2
     */
    public void function2(Object object1, Object object2);

    /**
     * This method is used to bridge between activity and the fragment
     * @param object1 optional argument 1
     * @param object2 optional argument 2
     */
    public void function3(Object object1, Object object2);

    /**
     * This method is used to bridge between activity and the fragment
     * @param object1 optional argument 1
     * @param object2 optional argument 2
     */
    public void function4(Object object1, Object object2);

    /**
     * This method is used to bridge between activity and the fragment
     * @param object1 optional argument 1
     * @param object2 optional argument 2
     */
    public void function5(Object object1, Object object2);
}
