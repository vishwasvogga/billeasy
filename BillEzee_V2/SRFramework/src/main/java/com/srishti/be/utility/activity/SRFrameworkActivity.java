package com.srishti.be.utility.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.srishti.be.utility.R;
import com.srishti.be.utility.SRAlertBuilder;
import com.srishti.be.utility.SRDisplayMessage;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.fragments.SRFrameworkLockScreenFragment;
import com.srishti.be.utility.timers.SRFrameworkUptimeTimer;

import java.util.HashMap;

import static com.srishti.be.utility.constants.SRApplicationConstants.SR_HOME_ACTIVITY_NAME;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_HOME_ACTIVITY_PACKAGE;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_INACTIVITY_AUTOLOGOUT_TIME_MS;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOCK_SCREEN_FRAGMENT_TAG;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MENU_TINT_ALPHA;

/**
 * this is the activity which is to be extended by all other activitys
 * used under this framework to make it subchild of framework activity
 */

public class SRFrameworkActivity extends AppCompatActivity implements SRFrameworkUptimeTimer.onSRFrameworkUptimeTimerCallback {
    private SRGetResources srGetResources=null;
    private DrawerLayout drawerLayout=null;
    private Toolbar toolbar=null;
    private LayoutInflater layoutInflater=null;
    private SRLog srLog=null;
    private HashMap<Integer,Boolean> isMenuOptionEnabled=null;
    private HashMap<Integer,Boolean> legacyIsMenuOptionEnabled=null;
    private NavigationView navigationView=null;
    private Activity activity=null;
    private String userId,userRole="";
    public boolean IS_DRAWER_ENABLED=true;
    private boolean IS_LOCKED=false;
    public SRFrameworkUptimeTimer srFrameworkUptimeTimer=null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialisation();
        configure();
        addActionListeners();
        srLog.debug("On create SR framework"+activity.getLocalClassName());
    }

    @Override
    protected void onDestroy() {
        SRLog.getInstance().debug("Activity destroyed"+activity.getLocalClassName());
        super.onDestroy();
    }

    /**
     * This method is used to initialise all the UI elements and class objects
     */
    private void initialisation(){
        activity=this;
        layoutInflater = getLayoutInflater();
        srGetResources = SRGetResources.getInstance(this);
        drawerLayout = (DrawerLayout) layoutInflater.inflate(R.layout.drawerlayout,null);
        setContentView(drawerLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        srLog = SRLog.getInstance();
        isMenuOptionEnabled= new HashMap<>();
        legacyIsMenuOptionEnabled = new HashMap<>();
        navigationView=(NavigationView) findViewById(R.id.navigation);
        srFrameworkUptimeTimer = SRFrameworkUptimeTimer.getInstance(activity);
    }

    /**
     * This method is used to configure the configurables
     */
    private void configure(){
        toolbar.setTitle(srGetResources.getTextFromResource(R.string.toolbar_tittle));
        toolbar.setTitleTextColor(srGetResources.getColorFromResource(R.color.toolbartittle));
        toolbar.setSubtitle("");
        toolbar.setSubtitleTextColor(Color.LTGRAY);
        toolbar.setTitleMarginStart((int)srGetResources.getDimensionFromResource(R.dimen.ToolbarTittleMarginStart));
        toolbar.inflateMenu(R.menu.toolbarmenu);
        setSupportActionBar(toolbar);
        //get user ID and role from application
        userId = SRFrameworkApplication.SR_LOGIN_USERNAME;
        userRole=getUserRoleFromCode(SRFrameworkApplication.SR_LOGIN_USERROLE);
    }

    /**
     * This method is used to set action listeners
     */
    public void addActionListeners(){
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId() == R.id.userManagement)
                {
                    //Call user management activity
                    if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        drawerLayout.closeDrawer(Gravity.RIGHT);
                    } else {
                        drawerLayout.openDrawer(Gravity.RIGHT);
                    }
                    //Call user managment activity
                    Intent callUserManagementActivity = new Intent();
                    callUserManagementActivity.setPackage("com.srishti.be.loginoperations.activitys");
                    callUserManagementActivity.setClassName(activity,"com.srishti.be.loginoperations.activitys.BELoginUserManagementActivity");
                    startActivity(callUserManagementActivity);

                }else if(item.getItemId() == R.id.MyAccount) {
                    //Call user management activity
                    if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        drawerLayout.closeDrawer(Gravity.RIGHT);
                    } else {
                        drawerLayout.openDrawer(Gravity.RIGHT);
                    }
                    //Call user managment activity
                    Intent myAccount = new Intent();
                    myAccount.setPackage("com.srishti.be.loginoperations.activitys");
                    myAccount.setClassName(activity,"com.srishti.be.loginoperations.activitys.BELoginMyAccountActivity");
                    startActivity(myAccount);

                }else{
                    onDrawerOptionSelected(item);
                }
                return true;
            }
        });
    }

    /**
     * This method will set the subtittle(MessageLine)
     * @param message Message string
     * @param Color color of the message
     */
    public void setMessageLine(String message,int Color){
        toolbar.setSubtitle(message);
        toolbar.setSubtitleTextColor(Color);
    }

    /**
     * This method is used to tint & Disable the menu item
     * @param itemId menu item ID
     */
    public void disableMenuItem(int itemId){
        try {
            MenuItem item = (MenuItem) toolbar.getMenu().findItem(itemId);
            item.getIcon().setAlpha(SR_MENU_TINT_ALPHA);
            isMenuOptionEnabled.put(item.getItemId(), false);
        }catch (Exception e){
            srLog.debug("Item not found to disable");
        }
    }

    /**
     * This method is used to enable the menu item
     * @param itemId menu item ID
     */
    public void enableMenuItem(int itemId){
        try {
            MenuItem item = (MenuItem) toolbar.getMenu().findItem(itemId);
            item.getIcon().setAlpha(255);
            isMenuOptionEnabled.put(item.getItemId(),true);
        }catch (Exception e){
            srLog.debug("Item not found to enable");
        }
    }

    /**
     * This method is used to tint all the menu items
     */
    public void disableAllMenuItem(){
        int numberOfItems = toolbar.getMenu().size();
        for(int i=0; i<numberOfItems;i++){
            disableMenuItem(toolbar.getMenu().getItem(i).getItemId());
        }
    }

    /**
     * This method is used to enable all the menu items
     */
    public void enableAllMenuItem(){
        int numberOfItems = toolbar.getMenu().size();
        for(int i=0; i<numberOfItems;i++){
            enableMenuItem(toolbar.getMenu().getItem(i).getItemId());
        }
    }


    @Override
    public void onBackPressed() {
        //If app is not locked then only enable back button
        //else disable back
        if(!IS_LOCKED) {
            super.onBackPressed();
        }
    }

    /**
     * This method is called when the otions menu is created
     * @param menu menu view
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        srLog.debug("On create optons menu SR framework");
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbarmenu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
         super.onPrepareOptionsMenu(menu);
        srLog.debug("On prepare optons menu SR framework");
        //Set drawer tittle
        TextView userid=(TextView) navigationView.getHeaderView(0).findViewById(R.id.T_NH_username);
        TextView userrole=(TextView) navigationView.getHeaderView(0).findViewById(R.id.T_NH_userRole);
        SRGetResources resources = SRGetResources.getInstance(this);
        String welcomeUser = resources.getTextFromResource(R.string.Welcome);
        welcomeUser =welcomeUser+ " "+this.userId+"!";
        userid.setText(welcomeUser);
        userrole.setText(userRole);

        //Enable disable Drawer
        if(!IS_DRAWER_ENABLED){
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
        }else{
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.END);
        }

        //Lock screen (close and open lock icon set)
        if(IS_LOCKED){
            menu.findItem(R.id.TM_Lock).setIcon(R.drawable.ic_lock_open_white_36dp);
        }else{
            menu.findItem(R.id.TM_Lock).setIcon(R.drawable.ic_lock_outline_white_36dp);
        }
        return true;
    }

    /**
     * This method is called on options in the toolbar selected
     * @param item menu item(option)
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            //Check if buttons are enabled
            boolean isEnabled = isMenuOptionEnabled.get(item.getItemId());
            //Return if disabled
            if (!isEnabled) {
                return true;
            }
        }catch (Exception e){
            srLog.debug("option menu state has not set , default is enabled");
        }
        //More button is pressed
        if(item.getItemId()==R.id.TM_MoreButton){
            if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            } else {
                drawerLayout.openDrawer(Gravity.RIGHT);
            }
            onMoreOptionOnToolbarSelected(item);
            //Log out button
        }else if(item.getItemId()==R.id.TM_Logout){
            //Ppop up alert dialog
            SRAlertBuilder srAlertBuilder = new SRAlertBuilder(this);
            srAlertBuilder.createAlertBuilder("",srGetResources.getTextFromResource(R.string.areYouSureToLogout));
            srAlertBuilder.addPositiveButton(srGetResources.getTextFromResource(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //On logout cancel the timer
                    srFrameworkUptimeTimer.cancelTImer();
                    //Clear all previous actiivyrt
                    Intent myAccount = new Intent();
                    myAccount.setPackage("com.srishti.be.loginoperations.activitys");
                    myAccount.setClassName(activity,"com.srishti.be.loginoperations.activitys.BELoginMainActivity");
                    startActivity(myAccount);
                    SRFrameworkActivity.this.activity.finish();
                }
            });
            srAlertBuilder.addNegetiveButton(srGetResources.getTextFromResource(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Do nothing
                }
            });
            srAlertBuilder.showDialogBox();

        }else if(item.getItemId()==R.id.TM_Home){
            //Call the next activity here , after login.
             Intent entryActivity = new Intent();
             entryActivity.setClassName(activity,SR_HOME_ACTIVITY_NAME);
             entryActivity.setPackage(SR_HOME_ACTIVITY_PACKAGE);
             startActivity(entryActivity);
        }else if(item.getItemId()==R.id.TM_Lock){
            //If previously unlocked state
            if(!IS_LOCKED) {
                //Lock drawer
                IS_DRAWER_ENABLED = !IS_DRAWER_ENABLED;
                //Enable lock
                IS_LOCKED = true;
                //Invalidate otions menu
                invalidateOptionsMenu();
                //get the previous states of buttons and save them
                copyHashmaps(isMenuOptionEnabled,legacyIsMenuOptionEnabled);
                //Disable all toolbar menu
                disableAllMenuItem();
                //Enable lock item
                enableMenuItem(R.id.TM_Lock);
                //Stop the timer
                //On signout cancel the timer
                srFrameworkUptimeTimer.cancelTImer();
                //Launch the lock screen fragment
                SRFragmentTransactionHelper.getInstance(this).ReplaceFragment(new SRFrameworkLockScreenFragment(),
                        SR_LOCK_SCREEN_FRAGMENT_TAG,R.id.mainFragmentContainer,SR_LOCK_SCREEN_FRAGMENT_TAG);
            }else{
                //Populate the pop up
                popUpUnlockScreen();
            }
        }else {
            onToolbarOptionsSelected(item);
        }
        return true;
    }

    /**
     * This method is to be called when the tool bar items are called
     * (This method is not called for more options button)
     */
    private void onToolbarOptionsSelected(MenuItem menuItem){
        srLog.debug("Menu item selected"+menuItem.getTitle());
    }

    /**
     * This method is called when more button on the toolbar is called
     * @param menuItem more button of toolbar
     */
    private void onMoreOptionOnToolbarSelected(MenuItem menuItem){
        srLog.debug("more option selected"+menuItem.getTitle());
    }

    /**
     * This method is called on an option in navigation drawer us selected
     * (This is not called when user management is selected)
     * @param menuItem menu item
     */
    private void onDrawerOptionSelected(MenuItem menuItem){
        srLog.debug("drawer option selected"+menuItem.getTitle());
    }

    /**
     * This fucntion is used to set the user id in the
     * drawer header
     * @param userId user id
     * @param userRole user role
     */
    public void setUserDetailsDrawer(final String userId,final String userRole){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SRFrameworkActivity.this.userId = userId;
                SRFrameworkActivity.this.userRole = userRole;
                invalidateOptionsMenu();
            }
        });
    }

    /**
     * THis method returns the user role string from user role int
     * @param userrole integer code
     * @return role in string
     */
    private String getUserRoleFromCode(int userrole){
        SRGetResources resources = SRGetResources.getInstance(this);
        switch (userrole){
            case 1: return resources.getTextFromResource(R.string.super_admin);
            case 2: return resources.getTextFromResource(R.string.admin);
            case 3: return resources.getTextFromResource(R.string.supervisor);
            case 4: return resources.getTextFromResource(R.string.operator);
        }
        return "";
    }

    /**
     * This method is used to copy tghe original hashmap to duplicate
     * (Customised to copy tool bar items state , cannot be used for general use)
     * @param hashmapOriginal original hashmap
     * @param hashMapDuplicate duplicate hasmap
     */
    private void copyHashmaps(HashMap<Integer,Boolean> hashmapOriginal,HashMap<Integer,Boolean> hashMapDuplicate){
        Menu menu = toolbar.getMenu();
        for(int i=0; i<menu.size();i++){
            MenuItem menuItem=menu.getItem(i);
            int menuId=menuItem.getItemId();
            hashMapDuplicate.put(menuId,hashmapOriginal.get(menuId));
        }
    }

    /**
     * This module creates a pop up and asks the user for password
     * to unlock the screen
     */
    private void popUpUnlockScreen(){
        final SRDisplayMessage displayMessage = new SRDisplayMessage(this);
        final SRAlertBuilder PUS_alertBuilder= new SRAlertBuilder(this);
        PUS_alertBuilder.createAlertBuilder();
        //Populate custom view
        View view = getLayoutInflater().inflate(R.layout.unlockpopupviewlayout,null);
        PUS_alertBuilder.setCustomView(view);
        //Get Edit text and button reference
        final EditText password = (EditText) view.findViewById(R.id.E_UPV_password);
        Button back =(Button) view.findViewById(R.id.B_UPV_Back);
        Button unlock=(Button) view.findViewById(R.id.B_UPV_unlock);
        TextView errorText =(TextView)view.findViewById(R.id.T_UPV_Message1);
        displayMessage.configure(true,2000,200,false,errorText);
        final TextInputLayout TIL_Password = (TextInputLayout) view.findViewById(R.id.TI_UPV_password);
        //set back button action
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PUS_alertBuilder.closeAlertDialog();
            }
        });

        unlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get user entered password
                String Spassword = password.getText().toString();
                //Get the user details
                if(SRFrameworkApplication.SR_LOGIN_USER_PASSWORD.equals(Spassword)) {
                    //remove the lock screen fragment
                    SRFragmentTransactionHelper.getInstance(SRFrameworkActivity.this).popBackStack();
                    //Lock drawer
                    IS_DRAWER_ENABLED = !IS_DRAWER_ENABLED;
                    //Enable lock
                    IS_LOCKED = false;
                    //Invalidate otions menu
                    invalidateOptionsMenu();
                    //get the previous states of buttons and take them to previous state
                    copyHashmaps(legacyIsMenuOptionEnabled, isMenuOptionEnabled);
                    Menu menu = toolbar.getMenu();
                    for (int i = 0; i < toolbar.getMenu().size(); i++) {
                        try {
                            MenuItem menuItem = menu.getItem(i);
                            int menuId = menuItem.getItemId();
                            boolean state = isMenuOptionEnabled.get(menuId);
                            if (state) {
                                //enable
                                enableMenuItem(menuId);
                                srLog.debug(menuItem.getTitle() + "enabled");
                            } else {
                                //disable
                                disableMenuItem(menuId);
                                srLog.debug(menuItem.getTitle() + "disabled");
                            }
                        } catch (Exception e) {
                            srLog.debug("SRFramework , menu Item not found");
                        }
                    }
                    //Start the auto logout timer
                    startAutoLogoutTimer(SR_INACTIVITY_AUTOLOGOUT_TIME_MS);
                    PUS_alertBuilder.closeAlertDialog();
                }else{
                    displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.passwordsdonotmatch),
                            Color.RED);
                }
            }
        });
        //populate the alert builder
        PUS_alertBuilder.showDialogBox();
    }

    /**
     * This method starts the auto logout timer
     */
    public void startAutoLogoutTimer(long timeout){
        srFrameworkUptimeTimer.startTimer(SRFrameworkActivity.this);
        srFrameworkUptimeTimer.setTimeout(timeout);
    }

    /**
     * This method is called whenever a touch on screen event is called.
     * then refresh the auto logout timer.
     * @param ev
     * @return true/false
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Your code here
       // srLog.debug("Touch event");
        srFrameworkUptimeTimer.resetTimer();
        return super.dispatchTouchEvent(ev);
    }


    /**
     * On auto logout timer expiry
     */
    @Override
    public void onTimerExpire() {
        srFrameworkUptimeTimer.cancelTImer();
        //logout
        //Call the next activity here , after login.
        Intent entryActivity = new Intent();
        entryActivity.setClassName(activity,"com.srishti.be.loginoperations.activitys.BELoginMainActivity");
        entryActivity.setPackage("com.srishti.be.loginoperations.activitys.BELoginMainActivity.BELoginMainActivity");
        startActivity(entryActivity);
    }
}
