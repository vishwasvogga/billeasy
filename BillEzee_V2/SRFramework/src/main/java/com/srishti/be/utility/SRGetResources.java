package com.srishti.be.utility;

import android.content.Context;
import android.content.res.Resources;

/**
 * This class provides methods to get the various resources.
 */

public class SRGetResources {
    private static SRGetResources srGetResources =null;
    private Context context=null;
    private Resources resources=null;

    /**
     * This method is used to get the singleton instance of the current class
     * @param context context
     * @return singleton instance of SRGetResources
     */
    public static SRGetResources getInstance(Context context){
        if(srGetResources ==null){
            srGetResources = new SRGetResources(context);
        }
        return srGetResources;
    }

    /**
     * This method is used to clear the previously created singleton instance.
     */
    public static void clearInstance(){
        if(srGetResources !=null){
            srGetResources =null;
        }
    }

    /**
     * This is the class constructor
     * @param context
     */
    private SRGetResources(Context context) {
        this.context = context;
        resources = this.context.getResources();
    }

    /**
     * This method is used to get the string resource from the
     * resources xml.
     * @param id Id of the string resource must be passed here.
     * @return text String
     */
    public String getTextFromResource(int id){
        return resources.getString(id);
    }

    /**
     * This method returns the dimension from resource files
     * @param id if of the dimension
     * @return dimension
     */
    public float getDimensionFromResource(int id){
        return resources.getDimension(id);
    }

    /**
     * This method returns color from resource
     * @param id Color ID
     * @return int color
     */
    public int getColorFromResource(int id){
        return resources.getColor(id);
    }
}
