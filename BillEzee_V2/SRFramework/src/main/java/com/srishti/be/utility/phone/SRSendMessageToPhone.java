package com.srishti.be.utility.phone;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.networkcomm.SRVolleyInterface;
import com.srishti.be.utility.networkcomm.SRVolleyNetworkCommunications;
import com.srishti.be.utility.networkcomm.SRVolleyRequestObject;
import com.srishti.be.utility.networkcomm.SRVolleyResponceObject;
import com.srishti.be.utility.networkcomm.SRVolleyStringRequest;

import java.util.HashMap;
import java.util.Random;

import static com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT;
import static com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_NETWORK_TIMEOUT_MS;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SEND_SMS_ACHAARIYA_PHONE_REQ_TAG;

/**
 * This class provides methods to send OTP to mobile phone
 */

public class SRSendMessageToPhone implements SRVolleyInterface,Response.Listener,Response.ErrorListener {
    private Context context=null;
    private onOtpSendingResponce onResponceOtp=null;


    /**
     * Constructor
     * @param context context
     */
    public SRSendMessageToPhone(Context context) {
        this.context=context;
    }


    public void sendOtpThruAchaaria(String url,onOtpSendingResponce onResponceOtp){
        //Set the input parametes to the volley request
        SRLog.getInstance().debug("Sending OTP request");
        SRVolleyRequestObject srVolleyRequestObject=new SRVolleyRequestObject();
        srVolleyRequestObject.body="";
        srVolleyRequestObject.headers=new HashMap<>();
        srVolleyRequestObject.errorListener=this;
        srVolleyRequestObject.listener=this;
        srVolleyRequestObject.method= Request.Method.GET;
        srVolleyRequestObject.url=url;
        srVolleyRequestObject.srVolleyInterface=this;
        this.onResponceOtp = onResponceOtp;
        //Volley string request object
        SRVolleyStringRequest srVolleyStringRequest = new SRVolleyStringRequest(srVolleyRequestObject);
        //Add to the request que
        RetryPolicy retryPolicy = new DefaultRetryPolicy(SR_NETWORK_TIMEOUT_MS,DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT);
        SRVolleyNetworkCommunications srVolleyNetworkCommunications = SRVolleyNetworkCommunications.getInstance(context);
        srVolleyNetworkCommunications.sendVolleyStringRequest(srVolleyStringRequest,SR_SEND_SMS_ACHAARIYA_PHONE_REQ_TAG,retryPolicy);
    }

    /**
     * On network responce
     * @param srVolleyResponceObject responce object
     */
    @Override
    public void onVolleyResponce(SRVolleyResponceObject srVolleyResponceObject) {
        SRLog.getInstance().debug("On OTP Responce"+srVolleyResponceObject.toString());
        onResponceOtp.onResponceOtp(srVolleyResponceObject.errorCode);
    }

    /**
     * This interface provides method to communicate
     * with the called party on OTP sent responce
     */
    public interface onOtpSendingResponce{
        public void onResponceOtp(int errorCode);
    }

    /**
     * This method returns a random number
     * @param numberOfDigits number of digits in OTP
     * @return otp as string
     */
    public String getRandomOtp(int numberOfDigits){
        if(numberOfDigits<0 || numberOfDigits >8){
            numberOfDigits=4;
        }
        Double random = new Random().nextDouble();
        Double random1 = random*(10^numberOfDigits);
        SRLog.getInstance().debug("random1 "+random1);
        char[] random2 = String.valueOf(random1).toCharArray();
        String random3="";
        for(int i=0; i<random2.length;i++){
            if(random2[i] != '.'){
                random3=random3+random2[i];
            }
        }
        return random3.substring(random3.length()-numberOfDigits,random3.length());
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {

    }
}
