package com.srishti.be.utility;

import android.app.Activity;
import android.content.Context;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

/**
 * This class provides method to display the flashing messages
 */

public class SRDisplayMessage {
    private Animation anim=null;
    private Runnable displayMessage=null;
    private boolean shallFlash,shallFlashContinusly=false;
    private int blinckTimeMs=1000;
    private int blinckFrequency=200;
    private TextView messageView=null;
    private Context context=null;

    /**
     * Public constructor
     * @param context context
     */
    public SRDisplayMessage(Context context) {
        this.context = context;
    }

    /**
     * This method is used to configure the blinking
     * @param shallFlash shall the message line flash
     * @param blinkTimeMs time of the blinking in milisecs
     * @param blinkFrequencyMs time of the one blink
     * @param shallFlashContinusly shall flash infinitely
     * @param messageView text view on which the message shall be dislpayed
     */
    public void configure(boolean shallFlash, int blinkTimeMs, int blinkFrequencyMs, boolean shallFlashContinusly, TextView messageView){
        this.shallFlash=shallFlash;
        this.shallFlashContinusly=shallFlashContinusly;
        this.blinckFrequency = blinkFrequencyMs;
        this.blinckTimeMs=blinkTimeMs;
        this.messageView=messageView;
    }

    /**
     * this mehod is used to display the message
     * shall be invoked only after config
     * @param message
     */
    public void displayMessage(final String message,final int Color){
        //Prepare the animation
        if(shallFlash && !shallFlashContinusly){
            anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(blinckFrequency); //You can manage the time of the blink with this parameter
            anim.setStartOffset(0);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount((int)(blinckTimeMs/blinckFrequency));
        }else if(shallFlash){
            anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(blinckFrequency); //You can manage the time of the blink with this parameter
            anim.setStartOffset(0);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
        }else{
            anim=null;
        }
        if(anim!=null){
            anim.start();
        }
        //Runnable
        displayMessage = new Runnable() {
            @Override
            public void run() {
                if(anim != null){
                    messageView.clearAnimation();
                    messageView.setAnimation(anim);
                    messageView.setText(message);
                    messageView.setTextColor(Color);
                }else{
                    messageView.setText(message);
                    messageView.setTextColor(Color);
                }
            }
        };
        ((Activity)context).runOnUiThread(displayMessage);
    }

    /**
     * This method sets the visibility to the view
     * @param visibility VIEW.visible,inv,gone
     */
    public void setVisibility(final int visibility){
        //Runnable
        displayMessage = new Runnable() {
            @Override
            public void run() {
                messageView.setVisibility(visibility);
            }
        };
        ((Activity)context).runOnUiThread(displayMessage);
    }

}
