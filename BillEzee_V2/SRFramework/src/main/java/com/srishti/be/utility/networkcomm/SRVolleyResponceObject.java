package com.srishti.be.utility.networkcomm;

import java.util.Arrays;
import java.util.Map;

/**
 * This method contains the responce parameters
 */

public class SRVolleyResponceObject {
    /**
     * Headers
     */
    public Map<String,String> headers=null;
    /**
     * Body
     */
    public byte[] body=null;
    /**
     * Error code
     */
    public int errorCode=-1;
    /**
     * network round trip timw
     */
    public long networkRoundTripTime=-1;

    @Override
    public String toString() {
        return "SRVolleyResponceObject{" +
                "headers=" + headers +
                ", body=" + Arrays.toString(body) +
                ", errorCode=" + errorCode +
                ", networkRoundTripTime=" + networkRoundTripTime +
                '}';
    }
}
