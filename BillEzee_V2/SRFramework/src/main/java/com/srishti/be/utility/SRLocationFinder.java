package com.srishti.be.utility;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

/**
 * This class is used to get the location of the device
 * along with long and latt
 */

public class SRLocationFinder extends Service implements LocationListener
{
    private final Context mContext;
    private static SRLocationFinder srLocationFinder=null;

    // flag for GPS status
    private boolean isGPSEnabled = false;

    // flag for network status
    private boolean isNetworkEnabled = false;

    private boolean canGetLocation = false;

    private Location location=null; // location
    private double latitude; // latitude
    private double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    /**
     * This method is used to get the singleton instance of this class
     * @param context context
     * @return SIngleton instance of SRLocationFinder
     */
    public static SRLocationFinder getInstance(Context context){
        if(srLocationFinder==null){
            srLocationFinder = new SRLocationFinder(context);
        }
        return srLocationFinder;
    }

    /**
     * This method is used to clear the previously created singketon instance
     */
    public static void clearInstance(){
        srLocationFinder=null;
    }

    public SRLocationFinder(Context context) {
        this.mContext = context;
        getLocation();
    }


    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            SRLog.getInstance().debug("Is network enabled : "+isNetworkEnabled);
            if (!isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            SRLog.getInstance().debug("LocationError : "+e);
        }

        return location;
    }

    public String getLocationName(){
        Geocoder geocoder = null;
        String result="Not Available";
        try{
        List<Address> list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        if (list != null & list.size() > 0) {
            Address address = list.get(0);
            result = address.getLocality();
            return result;
        }}catch(Exception e){
            SRLog.getInstance().debug("Location error"+e);
        }
        return result;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
