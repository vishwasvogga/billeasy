package com.srishti.be.utility.constants;

/**
 * This method contain the modules names
 */

public class SRCModuleNames {
    /**
     * All keys are iinternally converted to lowercase
     */
    public static final String SRCBELoginChangeCredentialsFragment="SRCBELoginChangeCredentialsFragment";
    public static final String SRCBELoginDeleteAccountFlag="SRCBELoginDeleteAccountFlag";
    public static final String SRCBELoginRegisterNewUserFragment="SRCBELoginRegisterNewUserFragment";
    public static final String SRCBELoginUserManagementMenuFragment="SRCBELoginUserManagementMenuFragment";
    public static final String SRCBELoginViewUsersFragment="SRCBELoginViewUsersFragment";
    public static final String SRCBELoginEditUsersFlag="SRCBELoginEditUsersFlag";
    public static final String SRCBELoginDeleteUsersFlag="SRCBELoginDeleteUsersFlag";
    public static final String SRCBELoginResetUsersFlag="SRCBELoginResetUsersFlag";
}
