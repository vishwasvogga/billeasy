package com.srishti.be.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

/**
 * This class is used to build the
 * alert dialog box
 */

public class SRAlertBuilder {
    private Context context=null;
    private AlertDialog.Builder alertDialog=null;
    private AlertDialog alertbox=null;

    /**
     * This is the public ocnstructor
     * @param context context
     */
    public SRAlertBuilder(Context context) {
        this.context = context;
    }

    /**
     * This method is used to add a positive butotn to the
     * alert dialog box
     * @param positiveButton text on the button
     * @param onClickListener onclick listener of the positive button
     */
    public void addPositiveButton(final String positiveButton,final DialogInterface.OnClickListener onClickListener){
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertbox.setButton(DialogInterface.BUTTON_POSITIVE,positiveButton,onClickListener);
            }
        });
    }

    /**
     * This method is used to add a negetive butotn to the
     * alert dialog box
     * @param negetiveButton text on the button
     * @param onClickListener onclick listener of the positive button
     */
    public void addNegetiveButton(final String negetiveButton,final DialogInterface.OnClickListener onClickListener){
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertbox.setButton(DialogInterface.BUTTON_NEGATIVE,negetiveButton,onClickListener);
            }
        });
    }

    /**
     * This method is used to create the dialog alert box
     * @param tittle tittle of the alert box
     * @param message message of te alert box
     */
    public void createAlertBuilder(final String tittle,final String message){
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog = new AlertDialog.Builder(context);
                alertbox = alertDialog.create();
                alertbox.setCancelable(false);
                alertbox.setTitle(tittle);
                alertbox.setMessage(message);
            }
        });
    }

    /**
     * This method is used to craete a alert dialog box without tittle or
     * message
     */
    public void createAlertBuilder(){
        SRLog.getInstance().debug("creating the dialog box");
        Runnable showDialog = new Runnable() {
            @Override
            public void run() {
                alertDialog = new AlertDialog.Builder(context);
                alertbox = alertDialog.create();
                alertbox.setCancelable(false);
            }
        };
        runOnUiThread(showDialog);
    }

    /**
     * This method is used to show the dialog box
     */
    public void showDialogBox(){
        SRLog.getInstance().debug("Showing the dialog box");
        Runnable showDialog = new Runnable() {
            @Override
            public void run() {
                SRLog.getInstance().debug("Showing the dialog box-executing");
                alertbox.show();
            }
        };
        runOnUiThread(showDialog);
    }

    /**
     * This method is used to set custom view to the
     * alert builder
     * @param view custom view to be set
     */
    public void setCustomView(final View view){
        Runnable showDialog = new Runnable() {
            @Override
            public void run() {
                SRLog.getInstance().debug("setting view");
                alertbox.setView(view);
            }
        };
        runOnUiThread(showDialog);


    }

    /**
     * This method is used to close the alert box
     */
    public void closeAlertDialog(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(alertbox!=null) {
                    alertbox.cancel();
                }
            }
        };
        runOnUiThread(runnable);

    }

    /**
     * This method will run the runnables in the uI thread
     * @param runnable
     */
    private void runOnUiThread(Runnable runnable){
        ((Activity)context).runOnUiThread(runnable);
    }


}
