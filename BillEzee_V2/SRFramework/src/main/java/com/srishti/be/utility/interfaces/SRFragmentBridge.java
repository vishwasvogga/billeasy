package com.srishti.be.utility.interfaces;

/**
 * This interface is used to bridge between the main activity and the fragment
 */

public interface SRFragmentBridge {
    /**
     * This method is used to bridge between activity and the fragment
     * @param object1 optional argument 1
     * @param object2 optional argument 2
     */
    public void onBackPressed(Object object1, Object object2);


    /**
     * This method is called when the menu itme in the frame is clicked(Excluding More button)
     * @param menuItemId ID of the menu
     */
    void onMenuSelected(int menuItemId,int menu);

    /**
     * This method is called when the more button is pressed
     */
    void onMoreMenuSelected();
}
