package com.srishti.be.utility.networkcomm;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.Volley;

/**
 * This class provides methods which are used for the network communication
 */

public class SRVolleyNetworkCommunications {
    private static SRVolleyNetworkCommunications srVolleyNetworkCommunications =null;
    private Context context=null;
    private RequestQueue requestQueue=null;

    /**
     * This mis private constructor
     * @param context context
     */
    private SRVolleyNetworkCommunications(Context context) {
        this.context=context;
        requestQueue = Volley.newRequestQueue(context);
    }

    /**
     * This method is used to get the singleton instance of this class.
     * @param context context
     * @return singleton instance of SRVolleyNetworkCommunications
     */
    public static SRVolleyNetworkCommunications getInstance(Context context){
        if(srVolleyNetworkCommunications ==null){
            srVolleyNetworkCommunications = new SRVolleyNetworkCommunications(context);
        }
        return srVolleyNetworkCommunications;
    }

    /**
     * This method is used to clear the
     * previously created singleton instance
     */
    public static void clearInstance(){
        if(srVolleyNetworkCommunications !=null){
            srVolleyNetworkCommunications = null;
        }
    }

    /**
     * This method is used to add the request to the que
     * @param request Request
     * @param tag tag of the request
     * @param retryPolicy retry policy
     */
    public void sendVolleyStringRequest(@NonNull Request request,@NonNull String tag,@NonNull RetryPolicy retryPolicy){
        request.setTag(tag);
        request.setRetryPolicy(retryPolicy);
        requestQueue.add(request);
    }

    /**
     * This method is used to cacel all the request with a tag
     * @param tag tag of the request
     */
    public void cancelRequestByTag(String tag){
        requestQueue.cancelAll(tag);
    }

    /**
     * This method is used to create custom request que
     * This must be only called before all the requests or
     * after all the request are completed
     * @param context context
     * @param httpStack custom http stack
     */
    public void createNewRequestQue(Context context, HttpStack httpStack){
        requestQueue = Volley.newRequestQueue(context,httpStack);
    }
}
