package com.srishti.be.utility.mail;

/**
 * This class contains objects needed for the mail sending
 */

public class SRSendMailRequestObject {
    /**
     * Tittle of the mail
     */
    public String tittle="";
    /**
     * This is the message to be sent to
     * receiver
     */
    public String message="";
    /**
     * This is the receiver's mail ID
     */
    public String receiverMailId="";
    /**
     * This is sender's mail id
     */
    public String senderMailId="";
    /**
     * this is sender's mailId's password
     */
    public String senderMailIdPassword="";
    /**
     * mail host
     */
    public String mailHost="";
    /**
     * mail port
     */
    public String mailPort="";
}
