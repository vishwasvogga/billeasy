package com.srishti.be.utility.mail;

import android.content.Context;

import com.srishti.be.utility.SRLog;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * This class provides methods to send messages to mail
 */

public class SRSendMessageToMail {
    private Context context=null;

    /**
     * Constructor
     * @param context context
     */
    public SRSendMessageToMail(Context context) {
        this.context =context;
    }

    /**
     * This method is used to send mail to a recipent
     * @param srSendMailRequestObject contains all the parameters needed to send a mail
     */
    public void sendMail(final SRSendMailRequestObject srSendMailRequestObject){
        Properties mailBoxProperty = new Properties();
        mailBoxProperty.put("mail.smtp.auth", "true");
        mailBoxProperty.put("mail.smtp.starttls.enable", "true");
        mailBoxProperty.put("mail.smtp.host", srSendMailRequestObject.mailHost);
        mailBoxProperty.put("mail.smtp.port",srSendMailRequestObject.mailPort );

        Session session = Session.getInstance(mailBoxProperty, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(srSendMailRequestObject.senderMailId, srSendMailRequestObject.senderMailIdPassword);
            }
        });

        try {
            MimeMessage e = new MimeMessage(session);
            e.setFrom(new InternetAddress(srSendMailRequestObject.senderMailId));
            e.setRecipients(Message.RecipientType.TO, InternetAddress.parse(srSendMailRequestObject.receiverMailId));
            e.setSubject(srSendMailRequestObject.tittle);
            e.setText(srSendMailRequestObject.message);
            Transport.send(e);
        } catch (MessagingException var9) {
            SRLog.getInstance().debug("Mail sending failed"+var9.getMessage());
            var9.printStackTrace();
        } catch (Exception var10) {
            SRLog.getInstance().debug("Mail sending failed"+var10.getMessage());
            var10.printStackTrace();
        }
    }
}
