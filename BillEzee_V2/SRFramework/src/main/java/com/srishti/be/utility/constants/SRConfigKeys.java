package com.srishti.be.utility.constants;

/**
 * This has the constants related to configuration
 */

public class SRConfigKeys {
    /**
     * All keys are iinternally converted to lowercase
     */
    public static final String SRCGeneralConfiguration  = "SRCGeneralConfiguration";
    public static final String SRCGeneral1="SRCGeneral1";
    public static final String SRCGeneral2="SRCGeneral2";
    public static final String SRCGeneral3="SRCGeneral3";
    public static final String SRCUserAccessConfiguration="SRCUserAccessConfiguration";
    public static final String SRCSuperAdmin="SRCSuperAdmin";
    public static final String SRCAdmin="SRCAdmin";
    public static final String SRCSupervisor="SRCSupervisor";
    public static final String SRCOperator="SRCOperator";
    public static final String SRCServerTimeout = "SRCServerTimeout";
    public static final String SRCautoLogoutTimeout="SRCautoLogoutTimeout";
    public static final String SRCMailHostOtp="SRCMailHostOtp";
    public static final String SRCMailPortOtp="SRCMailPortOtp";
}
