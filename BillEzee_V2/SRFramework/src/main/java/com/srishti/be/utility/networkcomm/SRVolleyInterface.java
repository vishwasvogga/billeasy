package com.srishti.be.utility.networkcomm;

/**
 * This is the interface between the volley and the application
 */

public interface SRVolleyInterface {
    public void onVolleyResponce(SRVolleyResponceObject srVolleyResponceObject);
}
