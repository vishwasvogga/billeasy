package com.srishti.be.utility;

import android.util.Log;

import static com.srishti.be.utility.constants.SRApplicationConstants.SR_IS_LOG_ENABLED;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOG_TITTLE;

/**
 * THis class provides the logging methods
 */

public class SRLog {
    private static SRLog SR_log =null;

    /**
     * To get the singletoninstance of thei class
     * @return SRLog
     */
    public static SRLog getInstance(){
        if(SR_log ==null){
            SR_log = new SRLog();
        }
        return SR_log;
    }

    /**
     * To clear the previously created singleton instance
     */
    public static void clearInstance(){
        SR_log = null;
    }

    /**
     * THis metodis used to log the message
     * @param message mesage which is to be logged
     */
    public void debug(String message){
        if(SR_IS_LOG_ENABLED) {
            Log.d(SR_LOG_TITTLE, message);
        }
    }

}
