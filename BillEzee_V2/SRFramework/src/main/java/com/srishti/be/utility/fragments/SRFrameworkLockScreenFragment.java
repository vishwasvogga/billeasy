package com.srishti.be.utility.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.srishti.be.utility.R;

/**
 * This is lock screen fragment
 */

public class SRFrameworkLockScreenFragment extends Fragment {
    private LinearLayout baseLayout=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        baseLayout = (LinearLayout) inflater.inflate(R.layout.lockscreenlayout,null);
        return baseLayout;
    }
}
