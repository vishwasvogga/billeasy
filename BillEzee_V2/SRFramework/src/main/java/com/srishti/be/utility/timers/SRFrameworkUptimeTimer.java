package com.srishti.be.utility.timers;

import android.content.Context;

import com.srishti.be.utility.SRLog;

import java.util.Timer;
import java.util.TimerTask;


/**
 * This class contains the method to create and access the uptime counter timer
 * which is used for the auto logout.
 */

public class SRFrameworkUptimeTimer {
    private static SRFrameworkUptimeTimer srFrameworkUptimeTimer=null;
    private Context context=null;
    private Timer mainTimer=null;
    private long systemTimeout=300000;
    private int timerInterval=1000; //1 Second
    private long systemUptime=0;
    private customTimerTask timerTask=null;
    private boolean isTimerRunning=false;
    private SRLog log=null;
    private boolean isPaused=false;

    /**
     * This method is used to get the singleton instance of this class
     * @param context context
     * @return singleton instance of  SRFrameworkUptimeTimer
     */
    public static SRFrameworkUptimeTimer getInstance(Context context){
        if(srFrameworkUptimeTimer==null){
            srFrameworkUptimeTimer = new SRFrameworkUptimeTimer(context);
        }
        return srFrameworkUptimeTimer;
    }

    /**
     * private Constructor
     * @param context
     */
    private SRFrameworkUptimeTimer(Context context){
        this.context = context;
        log=SRLog.getInstance();
    }

    /**
     * This method is used to set the system time out to the
     * timer
     * @param systemTimeout timeout in Milliseconds
     */
    public void setTimeout(long systemTimeout){
        this.systemTimeout = systemTimeout;
    }

    /**
     * This will restart the  timer after pause
     */
    public void restartTimer(){
       // log.debug("Timer is restarted");
        isPaused=false;
    }

    /**
     * This will reset the upcount
     */
    public void resetTimer(){
     //   log.debug("Timer is reset");
        systemUptime=0;
    }


    /**
     * This method will pause the upcount timer
     */
    public void pauseTimer(){
        isPaused=true;
    }

    /**
     * This method is used to stop the timer
     * inorder to start again we have to call startTimer()
     */
    public void cancelTImer(){
        if(mainTimer!=null) {
            timerTask.cancel();
            log.debug("Timer cancelled");
        }else{
            log.debug("Timer is null");
        }
    }


    /**
     * This method is used to start the timer
     * Once the timer expires it is cancelled automatically
     * so one has to call this method again.
     * @param onSRFrameworkUptimeTimerCallback callback interface
     * @return true - started else otherwise
     */
    public boolean startTimer(onSRFrameworkUptimeTimerCallback onSRFrameworkUptimeTimerCallback){
        if(!isTimerRunning) {
            mainTimer = new Timer();
            timerTask = new customTimerTask(onSRFrameworkUptimeTimerCallback);
            mainTimer.schedule(timerTask,0,timerInterval);
            isTimerRunning=true;
            isPaused=false;
            log.debug("Timer started");
            return true;
        }else{
            log.debug("Timer already running");
            return false;
        }
    }

    /**
     * Custom timer task
     */
    private class customTimerTask extends TimerTask{
        private onSRFrameworkUptimeTimerCallback onSRFrameworkUptimeTimerCallback=null;
        @Override
        public void run() {
            if(!isPaused) {
                systemUptime+=1000;//increament 1000 milliseconds
                //this method is called for every second.
               // log.debug("System up time : "+systemUptime);
                if(systemTimeout<=systemUptime){
                    onSRFrameworkUptimeTimerCallback.onTimerExpire();
                    //cancel timer
                    this.cancel();
                }
            }else{
                log.debug("Timer is paused");
            }

        }

        @Override
        public boolean cancel() {
            //reset all the values
            isTimerRunning=false;
            isPaused=false;
            systemUptime=0;
            return super.cancel();
        }

        protected customTimerTask(onSRFrameworkUptimeTimerCallback onSRFrameworkUptimeTimerCallback) {
          this.onSRFrameworkUptimeTimerCallback = onSRFrameworkUptimeTimerCallback;
        }
    }

    /**
     * This interface provides call back methods from this timer class
     */
    public interface onSRFrameworkUptimeTimerCallback{
        /**
         * This method is called in timer expiration
         */
        void onTimerExpire();
    }

}
