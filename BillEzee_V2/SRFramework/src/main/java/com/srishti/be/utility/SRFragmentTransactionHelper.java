package com.srishti.be.utility;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;

/**
 * This class provides method that helps fragment transactions
 */

public class SRFragmentTransactionHelper {
    private static SRFragmentTransactionHelper srFragmentTransactionHelper=null;
    private Context context=null;
    private FragmentManager fragmentManager=null;

    /**
     * This method is used to get the instance of this class.
     * @param context context
     * @return singleton instance of this class
     */
    public static SRFragmentTransactionHelper getInstance(Context context){
        srFragmentTransactionHelper = new SRFragmentTransactionHelper(context);
        return srFragmentTransactionHelper;
    }

    /**
     * This method is used to clear the previously created  instance
     */
    public static void clearInstance(){
        if(srFragmentTransactionHelper!=null){
            srFragmentTransactionHelper=null;
        }
    }


    /**
     * This is costructor of the class
     * @param context context
     */
    public SRFragmentTransactionHelper(Context context) {
        this.context = context;
        fragmentManager = ((Activity)this.context).getFragmentManager();
    }

    /**
     * This method is used to replace a fragment
     * @param fragment fragment to be replaced upon
     * @param backStack back stack name
     * @param containerId frame layout ID
     * @param fragmentTag tag of the fragment
     */
    public void ReplaceFragment(Fragment fragment,String backStack,int containerId,String fragmentTag){
        fragmentManager.beginTransaction().replace(containerId,fragment,fragmentTag).addToBackStack(backStack).commit();
    }

    /**
     * This method is used to replace a fragment without adding to back stack
     * @param fragment fragment to be replaced upon
     * @param containerId frame layout ID
     * @param fragmentTag tag of the fragment
     */
    public void ReplaceFragment(Fragment fragment,int containerId,String fragmentTag){
        fragmentManager.beginTransaction().replace(containerId,fragment,fragmentTag).commit();
    }

    /**
     * This method is used to pop back stack
     * @return true if ragment available at back stack else false.
     */
    public boolean popBackStack(){
        if(fragmentManager.getBackStackEntryCount()>0){
            fragmentManager.popBackStack();
            SRLog.getInstance().debug("BAckstackCount"+fragmentManager.getBackStackEntryCount());
            return true;
        }else{
            SRLog.getInstance().debug("nothing in backstack");
            return false;
        }
    }

    /**
     * Thsi method returns number of fragments at the back stack
     * @return integer
     */
    public int numberOfFragmentBackstack(){
        return fragmentManager.getBackStackEntryCount();
    }

    /**
     * This returns the fragment in the back stack by tag
     * @param tag string tag
     * @return fragment , null if not in back stack
     */
    public Fragment getFragmentInBackstackFromTag(String tag){
        return fragmentManager.findFragmentByTag(tag);
    }

    /**
     * This method removes all the fragments from the back stack
     * with the TAG
     * @param tag tag of the fragment
     */
    public void popBackStackInclusive(String tag){
        if(fragmentManager.getBackStackEntryCount()>0){
            fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        }else{
            //Do nothing as back stack is empty
        }
    }

    /**
     * THis method will clear the back stack
     */
    public void clearBackStack(){
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }




}
