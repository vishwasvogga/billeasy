package com.srishti.be.utility.constants;

import com.srishti.be.utility.interfaces.SRFragmentBridge;

/**
 * This class contains the application constants
 */

public class SRApplicationConstants {
    public static SRFragmentBridge srFragmentBridge =null;
    public static String SR_LOGIN_MAIN_FRAGMENT_TAG="Login main page tag";
    public static String SR_LOGIN_ADD_NEW_USER_FRAGMENT_TAG="Login add user page tag";
    public static String SR_LOGIN_ADD_VIEW_USERS_FRAGMENT_TAG="Login view users page tag";
    public static String SR_LOGIN_USER_MANAGEMENT_FRAGMENT_TAG="user management page tag";
    public static String SR_LOGIN_DELETE_FRAGMENT_TAG="user management delete page tag";
    public static String SR_LOGIN_RESET_FRAGMENT_TAG="user management reset page tag";
    public static String SR_MY_ACCOUNT_FRAGMENT_TAG="MY account page tag";
    public static String SR_LOCK_SCREEN_FRAGMENT_TAG="Lock screen page tag";
    public static String SR_CHANGE_CREDENTIALS_FRAGMENT_TAG="Change credentials page tag";
    public static final boolean SR_IS_LOG_ENABLED =true;
    public static final String SR_LOG_TITTLE ="SR_LOG";
    public static final String SR_SHARED_PREFRENCE_KEY ="SR_Shared_Prefrence";
    public static final String SR_LOGIN_ENCRYPTION_SECRET_KEY="SR_Login_encryption_secretKey";
    public static final String SR_LOGIN_ENCRYPTION_SECRET_KEY_LENGTH="SR_Login_encryption_secretKey_Length";
    public static final String SR_SEND_SMS_ACHAARIYA_PHONE_REQ_TAG="send otp thru achaaria";



    public static final String SR_ACHAARIYA_UID="537269736874694573646d";
    public static final String SR_ACHAARIYA_PIN="0339402fda567c234c54f286694f5bd8";
    public static final String SR_SRISHTI_MAIL_ID="infosrishtiesdm@gmail.com";
    public static final String SR_SRISHTI_MAIL_PASSWORD="info@srishti";
    public static final int SR_MENU_TINT_ALPHA=50;
    public static final float SR_BUTTON_TINT_ALPHA=0.3f;


    /**
     * Configuarables
     */
    public static  long SR_INACTIVITY_AUTOLOGOUT_TIME_MS=120000;
    public static  int SR_NETWORK_TIMEOUT_MS=20000;
    public static  String SR_MAIL_HOST="smtp.gmail.com";
    public static  String SR_MAIL_PORT="587";
    /**
     * Home activity
     */
    public static final String SR_HOME_ACTIVITY_NAME="com.srishti.be.loginoperations.activitys.SalesDummyActivity";
    public static final String SR_HOME_ACTIVITY_PACKAGE="com.srishti.be.loginoperations.activitys";
}
