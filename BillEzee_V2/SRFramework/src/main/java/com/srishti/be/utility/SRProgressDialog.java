package com.srishti.be.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * This class provides method to
 * create a progress dialog
 */

public class SRProgressDialog {
    private Context context=null;
    private ProgressDialog progressDialog=null;

    /**
     * constructor
     * @param context context
     */
    public SRProgressDialog(Context context) {
        this.context = context;
    }

    /**
     * This method is used to create the progress dialog
     * @param tittle tittle of the progress dialog
     * @param message message of the progress dialog
     */
    public void createProgressDialog(final String tittle,final String message){
        Runnable cpd = new Runnable() {
            @Override
            public void run() {
                progressDialog = new ProgressDialog(context);
                progressDialog.setTitle(tittle);
                progressDialog.setMessage(message);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        };
        runOnUiThread(cpd);
    }

    /**
     * This method is used to dismiss the showing
     * progress dialog
     */
    public void dismissProgressDialog(){
        SRLog.getInstance().debug("dismiss progressDialog..checking is showing");
        if(progressDialog !=null && progressDialog.isShowing()){
            SRLog.getInstance().debug("dismiss progressDialog..");
            Runnable dpd = new Runnable() {
                @Override
                public void run() {
                    SRLog.getInstance().debug("dismissing progressDialog..");
                    progressDialog.dismiss();
                    SRLog.getInstance().debug("dismissed progressDialog..");
                }
            };
            runOnUiThread(dpd);
        }
    }

    /**
     * This method will run the runnables in the uI thread
     * @param runnable runnable which is to be run
     */
    private void runOnUiThread(Runnable runnable){
        ((Activity)context).runOnUiThread(runnable);
    }


}
