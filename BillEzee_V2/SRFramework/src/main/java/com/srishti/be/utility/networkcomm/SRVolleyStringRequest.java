package com.srishti.be.utility.networkcomm;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

/**
 * This is the class for string request
 */

public class SRVolleyStringRequest extends StringRequest {
    /**
     * To hold the volley responce
     */
    private SRVolleyResponceObject volleyResponce=new SRVolleyResponceObject();
    /**
     * To hold the volley request
     */
    private SRVolleyRequestObject volleyRequest=null;

    /**
     * Constructor
     * @param srVolleyRequestObject SR Volley request object
     */
    public SRVolleyStringRequest(SRVolleyRequestObject srVolleyRequestObject) {
        super(srVolleyRequestObject.method, srVolleyRequestObject.url, srVolleyRequestObject.listener, srVolleyRequestObject.errorListener);
        this.volleyRequest = srVolleyRequestObject;
    }

    /**
     * This method is used to pass headers to the request
     * @return Map<String, String>
     * @throws AuthFailureError
     */
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return volleyRequest.headers;
    }

    /**
     * This is used to pass the body to the request
     * @return byte[]
     * @throws AuthFailureError
     */
    @Override
    public byte[] getBody() throws AuthFailureError {
        return volleyRequest.body.getBytes();
    }

    /**
     * This method is called at the event of error
     * @param volleyError volley Error object
     * @return VolleyError
     */
    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        if(volleyError.networkResponse!=null){
            volleyResponce.headers = volleyError.networkResponse.headers;
            volleyResponce.errorCode = volleyError.networkResponse.statusCode;
            volleyResponce.networkRoundTripTime=volleyError.networkResponse.networkTimeMs;
            volleyResponce.body=volleyError.networkResponse.data;
        }else{
            volleyResponce.headers = null;
            volleyResponce.errorCode = -1;
            volleyResponce.networkRoundTripTime=volleyError.getNetworkTimeMs();
            volleyResponce.body=null;
        }
        //PAss the data thru interface
        volleyRequest.srVolleyInterface.onVolleyResponce(volleyResponce);

        return super.parseNetworkError(volleyError);
    }



    /**
     * This method is called on responce from the server
     * @param response
     * @return Response<String>
     */
    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        volleyResponce.headers = response.headers;
        volleyResponce.errorCode = response.statusCode;
        volleyResponce.networkRoundTripTime=response.networkTimeMs;
        volleyResponce.body=response.data;

        //PAss the data thru interface
        volleyRequest.srVolleyInterface.onVolleyResponce(volleyResponce);

        return super.parseNetworkResponse(response);
    }


}
