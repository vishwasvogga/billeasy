package com.srishti.be.utility.fileoperation;

import android.content.Context;
import android.content.res.AssetManager;

import com.srishti.be.utility.SRLog;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * This class provides methods for CSV file operations
 */

public class SRFileOperations {
    private File file=null;
    private Context context=null;
    private SRLog srLog=null;
    private BufferedReader bufferedReader=null;
    /**
     * This method is used to get the instance of the class.
     * (This is not singleton implementation)
     * @param context context
     * @return SRFileOperations
     */
    public static SRFileOperations getInstance(Context context){
        return new SRFileOperations(context);
    }
    /**
     * This is constructor
     * @param context context
     */
    private SRFileOperations(Context context){
       this.context = context;
        srLog = SRLog.getInstance();
    }

    /**
     * This function is used to read the file
     * @param file
     */
    public void readFile(File file){
        this.file = file;
    }

    /**
     * This method is used to read the file from the storage
     * @param path
     * @return boolean true - success , false- failure
     */
    public boolean readFileFromStorage(String path){
        File file = new File(path);
        if(!file.exists()){
            // not a file
        }
        if(file.isDirectory()){
            //Not a file
        }

        return true;
    }

    /**
     * This method reads the file from the assests
     * @param name name of the file
     * @return boolean true success reading file
     */
    public boolean readFileFromAssets(String name){
        try {
            //get the assets
            AssetManager assetManager = context.getAssets();
            InputStream fileInputStream = assetManager.open(name);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            srLog.debug(e.getMessage());
            return false;
        }
    }

    /**
     * This method will return the lines
     * @return hashmap key-line number , value - line , null if error
     */
    public HashMap<Integer,String> getLinesFromFile(){
        HashMap<Integer,String> lines = new HashMap<>();
        try {
            String line = "";
            int lineNo=0;
            //go thru each line
            while ( (line = bufferedReader.readLine())!=null) {
                lineNo++;
                lines.put(lineNo,line);
               // srLog.debug("line no:"+lineNo+" "+line);
            }
            srLog.debug("file line read success");
        }catch (Exception e){
            srLog.debug("error getting lines from file"+e.getMessage());
            e.printStackTrace();
            return null;
        }
        return lines;
    }
}
