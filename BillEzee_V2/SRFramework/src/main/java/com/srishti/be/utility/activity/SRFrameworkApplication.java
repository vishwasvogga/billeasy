package com.srishti.be.utility.activity;

import android.app.Application;

import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRLocationFinder;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.config.SRGetConfiguration;
import com.srishti.be.utility.networkcomm.SRVolleyNetworkCommunications;
import com.srishti.be.utility.sharedpreference.SRSharedPrefrence;

/**
 * This application class is called along with the first activity
 */

public class SRFrameworkApplication extends Application {
    private SRLog srLog=null;
    public static String SR_LOGIN_USERNAME="";
    public static int SR_LOGIN_USERROLE=-1;
    public static String SR_LOGIN_USER_PHONE="";
    public static String SR_LOGIN_USER_MAIL="";
    public static String SR_LOGIN_USER_PASSWORD="";

    @Override
    public void onCreate() {
        super.onCreate();
        srLog = SRLog.getInstance();
        srLog.debug("Application created");
        //Clear all singleton classes
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        //Clear singleton classes
        SRLog.clearInstance();
        SRVolleyNetworkCommunications.clearInstance();
        SRGetResources.clearInstance();
        SRLocationFinder.clearInstance();
        SRSharedPrefrence.clearInstance();
        SRGetConfiguration.clearInsatnce();
        srLog.debug("Application terminated");
    }
}
