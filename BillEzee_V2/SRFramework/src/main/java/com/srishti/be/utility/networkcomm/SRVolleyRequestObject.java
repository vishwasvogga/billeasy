package com.srishti.be.utility.networkcomm;

import com.android.volley.Request;
import com.android.volley.Response;

import java.util.Map;

/**
 * This class contains the parameters to be passed during the volley request
 */

public class SRVolleyRequestObject {
    /**
     * Request method
     */
    public int method= Request.Method.GET;
    /**
     * URL of the request
     */
    public String url="";
    /**
     * Body of the request
     */
    public String body="";
    /**
     * Headers of the request
     */
    public Map<String,String> headers=null;
    /**
     * SR Volley interface to return the volley
     * Result
     */
    public SRVolleyInterface srVolleyInterface=null;
    /**
     * Volley responce listner
     */
    public Response.Listener<String> listener=null;
    /**
     * Volley error listener
     */
    public Response.ErrorListener errorListener=null;

}
