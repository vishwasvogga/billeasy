package com.srishti.be.utility.config;

import android.content.Context;

import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.constants.SRConfigKeys;
import com.srishti.be.utility.fileoperation.SRFileOperations;

import java.util.HashMap;

import static com.srishti.be.utility.constants.SRCModuleNames.SRCBELoginChangeCredentialsFragment;
import static com.srishti.be.utility.constants.SRCModuleNames.SRCBELoginDeleteAccountFlag;
import static com.srishti.be.utility.constants.SRCModuleNames.SRCBELoginDeleteUsersFlag;
import static com.srishti.be.utility.constants.SRCModuleNames.SRCBELoginRegisterNewUserFragment;
import static com.srishti.be.utility.constants.SRCModuleNames.SRCBELoginResetUsersFlag;
import static com.srishti.be.utility.constants.SRCModuleNames.SRCBELoginUserManagementMenuFragment;
import static com.srishti.be.utility.constants.SRCModuleNames.SRCBELoginViewUsersFragment;
import static com.srishti.be.utility.constants.SRConfigKeys.SRCMailHostOtp;
import static com.srishti.be.utility.constants.SRConfigKeys.SRCMailPortOtp;
import static com.srishti.be.utility.constants.SRConfigKeys.SRCServerTimeout;
import static com.srishti.be.utility.constants.SRConfigKeys.SRCautoLogoutTimeout;

/**
 * This clas provides methods to get the configuration values
 * mostly used in BELoginMainactivity , the values are refrenced throughout the
 * program as and when needed.
 */

public class SRGetConfiguration {
    private static SRGetConfiguration srGetConfiguration=null;
    private Context context=null;
    private HashMap<Integer,String> firstWordInRow=new HashMap<>();
    private HashMap<Integer,String[]> commaSeperatedLines=new HashMap<>();
    private SRLog log = null;
    //to store line number of config value containing rows
    private int SRCGeneral1LineNo=0;
    private int SRCGeneral2LineNo=0;
    private int SRCGeneral3LineNo=0;
    private int SRCUserAccessConfigurationLineNo=0;
    private int SRCUsuperAdminLineNo=0;
    private int SRCUAdminLineNo=0;
    private int SRCUsuperVisorLineNo=0;
    private int SRCUoperatorLineNo=0;
    //Config values
    public long networkTimeout=30000;
    public long autoLogoutTimeout=300000;
    public String mailHost="smtp.gmail.com";
    public String mailPort="587";
    //Access to modules
    public boolean accessToSRCBELoginChangeCredentialsFragment=true;
    public boolean accessToSRCBELoginDeleteAccountFlag=false;
    public boolean accessToSRCBELoginRegisterNewUserFragment=false;
    public boolean accessToSRCBELoginUserManagementMenuFragment=true;
    public boolean accessToSRCBELoginViewUsersFragment=true;
    public boolean accessToSRCBELoginDeleteUsersFlag=false;
    public boolean accessToSRCBELoginResetUsersFlag=false;

    private String[] moduleNameKeys=null;
    private String[] superAdminRights=null;
    private String[] superVisorRights=null;
    private String[] operatorRights=null;
    private String[] adminRights=null;

    public boolean IS_CONFIG_SUCCESS=true;



    /**
     * This method is used to get the singleton instance of this class.
     * @param context context
     * @return SRGetConfiguration singleton instance
     */
    public static SRGetConfiguration getInstance(Context context){
        if(srGetConfiguration==null){
            srGetConfiguration= new SRGetConfiguration(context);
        }
        return srGetConfiguration;
    }

    /**
     * Constructor
     * @param context context
     */
    public SRGetConfiguration(Context context) {
        this.context=context;
        log = SRLog.getInstance();
    }

    /**
     * This modules is used to clear the singleton instance
     */
    public static void clearInsatnce(){
        if(srGetConfiguration!=null){
            srGetConfiguration=null;
        }
    }

    /**
     * This will load the cnfig CSV file from the assets
     * @return true-success else failure
     */
    public boolean loadConfigFromAssets(){
        HashMap<Integer,String> configFileRows=null;
        SRFileOperations srFileOperations=SRFileOperations.getInstance(context);
        boolean ret =srFileOperations.readFileFromAssets("Config.csv");
        if(!ret){
            //csv reading failed
            IS_CONFIG_SUCCESS=false;
            return false;
        }
        configFileRows = srFileOperations.getLinesFromFile();
        if(configFileRows==null){
            //csv read failed
            IS_CONFIG_SUCCESS=false;
            return false;
        }
        //If csv read success then
        //parse at comma
        for (int i=0; i<configFileRows.size();i++){
            String line = configFileRows.get(i+1);  //i+1 beacuse line no starts with 1
            line = line.replaceAll("\\s+","");  //remove all empty space
            String[] parsedAtComma=line.split(",");  //Split at comma
            commaSeperatedLines.put(i+1,parsedAtComma); //store in hashmap
            firstWordInRow.put(i+1,parsedAtComma[0]); //Store the first words (factory defined keys)
        }
        //get the line numbers of config values start
        getLineNumbersOfConfigValueStart();
        //set general config 1
        setGeneral1ConfigValues();
        //set general config 2
        setGeneral2ConfigValues();
        //set general config 3
        setGeneral3ConfigValues();
        //set access rights config values
        setSRCUserAccessConfigurationValues();
        return true;
    }

    /**
     * This method is used to extract the line numbers
     * in which config values has stored
     */
    private void getLineNumbersOfConfigValueStart(){
        //Go thru each first line
        for(int i=0;i<firstWordInRow.size();i++){
            int lineNo=i+1;
            //check for general 1
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCGeneral1)){
                SRCGeneral1LineNo=lineNo;
            }
            //check for general 2
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCGeneral2)){
                SRCGeneral2LineNo=lineNo;
            }
            //check for general 3
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCGeneral3)){
                SRCGeneral3LineNo=lineNo;
            }
            //check for SRCUserAccessConfiguration
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCUserAccessConfiguration)){
                SRCUserAccessConfigurationLineNo=lineNo;
            }
            //check for superadmin line
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCSuperAdmin)){
                SRCUsuperAdminLineNo=lineNo;
            }
            //check for admin line
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCAdmin)){
                SRCUAdminLineNo=lineNo;
            }
            //check for supervisor line
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCSupervisor)){
                SRCUsuperVisorLineNo=lineNo;
            }
            //check for operator line
            if(firstWordInRow.get(lineNo).equals(SRConfigKeys.SRCOperator)){
                SRCUoperatorLineNo=lineNo;
            }
        }
    }


    /**
     * This method sets the general 1 config values
     */
    private void setGeneral1ConfigValues(){
        //set values indiviually
        try {
            HashMap<String,String> SRCGeneral1=new HashMap<>();
            String[] ScommaSeperatedValues = commaSeperatedLines.get(SRCGeneral1LineNo);
            for(int i=1; i<=(ScommaSeperatedValues.length/2);i++) {
                SRCGeneral1.put(ScommaSeperatedValues[(i*2)-1],ScommaSeperatedValues[(i*2)]);
            }
            this.networkTimeout = Long.parseLong(SRCGeneral1.get(SRCServerTimeout));
            this.autoLogoutTimeout = Long.parseLong(SRCGeneral1.get(SRCautoLogoutTimeout));
            this.mailHost = SRCGeneral1.get(SRCMailHostOtp);
            this.mailPort = SRCGeneral1.get(SRCMailPortOtp);
        }catch (Exception e){
            IS_CONFIG_SUCCESS=false;
            log.debug("Error in setting general 1 values");
            e.printStackTrace();
        }
    }

    /**
     * This method sets the general 2 config values
     */
    private void setGeneral2ConfigValues(){
        try {
            HashMap<String, String> SRCGeneral2 = new HashMap<>();
            String[] ScommaSeperatedValues = commaSeperatedLines.get(SRCGeneral2LineNo);
            for (int i = 1; i <= (ScommaSeperatedValues.length / 2); i++) {
                SRCGeneral2.put(ScommaSeperatedValues[(i * 2) - 1], ScommaSeperatedValues[(i * 2)]);
            }
        }catch (Exception e){
            IS_CONFIG_SUCCESS=false;
            log.debug("Error in setting general 2 values");
            e.printStackTrace();
        }
    }

    /**
     * This method sets the general 3 config values
     */
    private void setGeneral3ConfigValues(){
        try {
            HashMap<String, String> SRCGeneral3 = new HashMap<>();
            String[] ScommaSeperatedValues = commaSeperatedLines.get(SRCGeneral3LineNo);
            for (int i = 1; i <= (ScommaSeperatedValues.length / 2); i++) {
                SRCGeneral3.put(ScommaSeperatedValues[(i * 2) - 1], ScommaSeperatedValues[(i * 2)]);
            }
        }catch (Exception e){
            IS_CONFIG_SUCCESS=false;
            log.debug("Error in setting general 3 values");
            e.printStackTrace();
        }
    }

    /**
     * This is used to set SRCUserAccessConfigurationValues
     */
    private void setSRCUserAccessConfigurationValues(){
        try {
            moduleNameKeys = commaSeperatedLines.get(SRCUserAccessConfigurationLineNo);
            int noOfModules = moduleNameKeys.length;
            superAdminRights = commaSeperatedLines.get(SRCUsuperAdminLineNo);
            adminRights = commaSeperatedLines.get(SRCUAdminLineNo);
            superVisorRights = commaSeperatedLines.get(SRCUsuperVisorLineNo);
            operatorRights = commaSeperatedLines.get(SRCUoperatorLineNo);
        }catch (Exception e){
            IS_CONFIG_SUCCESS=false;
            log.debug("Could not load access rights , keys missing");
            e.printStackTrace();
        }
    }

    /**
     * This will set the module access level
     * from config , to public variable
     * @param userRole 1-superadmin .. 4-operator
     */
    public void setSRCUserAccess(int userRole){
        String[] values= null;
        switch (userRole){
            case 1 : values = superAdminRights;
                               break;
            case 2 : values = adminRights;
                               break;
            case 3 : values = superVisorRights;
                               break;
            case 4 : values = operatorRights;
                               break;
        }
        if(values==null){
            IS_CONFIG_SUCCESS=false;
            log.debug("Could not load user access");
            return;
        }
        //form the hashmap with key and value
        HashMap<String,String> userAccessValueKey = new HashMap<>();
        for(int i=0; i<values.length;i++){
            userAccessValueKey.put(moduleNameKeys[i],values[i]);
        }
        //update the boolean flags
        if(stringToBoolean(userAccessValueKey.get(SRCBELoginChangeCredentialsFragment))){
            accessToSRCBELoginChangeCredentialsFragment=true;
        }else{
            accessToSRCBELoginChangeCredentialsFragment=false;
        }

        if(stringToBoolean(userAccessValueKey.get(SRCBELoginDeleteAccountFlag))){
            accessToSRCBELoginDeleteAccountFlag=true;
        }else{
            accessToSRCBELoginDeleteAccountFlag=false;
        }

        if(stringToBoolean(userAccessValueKey.get(SRCBELoginRegisterNewUserFragment))){
            accessToSRCBELoginRegisterNewUserFragment=true;
        }else{
            accessToSRCBELoginRegisterNewUserFragment=false;
        }

        if(stringToBoolean(userAccessValueKey.get(SRCBELoginUserManagementMenuFragment))){
            accessToSRCBELoginUserManagementMenuFragment=true;
        }else{
            accessToSRCBELoginUserManagementMenuFragment=false;
        }

        if(stringToBoolean(userAccessValueKey.get(SRCBELoginViewUsersFragment))){
            accessToSRCBELoginViewUsersFragment=true;
        }else{
            accessToSRCBELoginViewUsersFragment=false;
        }

        if(stringToBoolean(userAccessValueKey.get(SRCBELoginDeleteUsersFlag))){
            accessToSRCBELoginDeleteUsersFlag=true;
        }else{
            accessToSRCBELoginDeleteUsersFlag=false;
        }

        if(stringToBoolean(userAccessValueKey.get(SRCBELoginResetUsersFlag))){
            accessToSRCBELoginResetUsersFlag=true;
        }else{
            accessToSRCBELoginResetUsersFlag=false;
        }
    }

    /**
     * This method converts the Y /N
     * to true ot false boolean
     * @param value value Y or N
     * @return boolean true/false
     */
    private boolean stringToBoolean(String value){
        if(value==null){
            IS_CONFIG_SUCCESS=false;
            log.debug("module Key not found setting false as default");
            return false;
        }
        if(value.equalsIgnoreCase("y")){
            return true;
        }else{
            return false;
        }
    }

}
