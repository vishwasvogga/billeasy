package com.srishti.be.utility;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * This class provides method to parse the XML.
 */

public class SRXmlUtil {

    /**
     * This function returns the value of attribute from the xml.
     * first element first attribute value
     * @param xml
     * @param attribute
     * @param element
     * @return value of the Attribute.
     */
    public String getAttributeValueFromXml(String xml, String attribute, String element) throws Exception{
        String attrValue="";
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc=docBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
        // Get the root element
        NodeList nodeList = doc.getElementsByTagName(element);
        if(nodeList.getLength()!=0){
            Node node = nodeList.item(0);
            Element rootElement =(Element) node;
            attrValue = rootElement.getAttribute(attribute);
            SRLog.getInstance().debug(attrValue);
        }
        //List attributes in the element
        return attrValue;
    }

    /**
     * This function is used to get the vaue of the elemebnt in the
     * XML. First element value.
     * @param xml XML Which is to be parsed
     * @param element  name of the element whose value is to be get
     * @return value of the element
     * @throws Exception
     */
    public String getElementValueFromXml(String xml, String element) throws Exception{
        String elementValue="";
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc=docBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
        // Get the root element
        NodeList nodeList = doc.getElementsByTagName(element);
        if(nodeList.getLength()!=0){
            Node node = nodeList.item(0);
            Element rootElement =(Element) node;
            elementValue = rootElement.getTextContent();
            SRLog.getInstance().debug(elementValue);
        }
        //If no elements found returns empty string
        //List attributes in the element
        return elementValue;
    }

}
