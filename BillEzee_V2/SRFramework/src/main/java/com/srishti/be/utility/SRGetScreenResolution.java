package com.srishti.be.utility;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * THis class returns the screen related parameters
 */

public class SRGetScreenResolution {
    private static SRGetScreenResolution screenResolution=null;

    /**
     * This method is used to get the singleton instance of this class.
     * @param context context
     * @return singleton instance of SRGetScreenResolution class.
     */
    public static SRGetScreenResolution getInstance(Context context){
        if(screenResolution==null){
            screenResolution= new SRGetScreenResolution(context);
        }
        return screenResolution;
    }

    /**
     * This method is used to clear the singleton instance
     */
    public static void clearInstance(){
        if(screenResolution!=null) {
            screenResolution = null;
        }
    }

    private Context context=null;
    /**
     * This is the class constructor
     * @param context pass the context
     */
    public SRGetScreenResolution(Context context) {
        this.context = context;
    }

    /**
     * THis method returns the screen height
     * @return screen height in pixels
     */
    public int getScreenHeight(){
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }

    /**
     * This method returns the screen width
     * @return screen width in pixels
     */
    public int getScreenWidth(){
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    /**
     * This method returns the toolbar height
     * @return tool bar height in pixels
     */
    public int getToolbarHeight(){
        int actionBarHeight=0;
        TypedValue tv = new TypedValue();
        if (((Activity)context).getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
             actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,((Activity)context).getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }
}
