package com.srishti.be.utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

/**
 * This class provides the methods for the count down timer
 */

public class SRCountDownTimer
{
    private Context context=null;
    private long timeout=0;
    private long elapsed_time_otp=0;
    private long C_Minutes,C_Seconds=0;
    private TextView textView=null;
    private CountDownTimer countDownTimer=null;
    private int textColorPrimary=Color.BLACK;
    private onTimerEvents onTimerEvents=null;

    /**
     * Public constructor
     *
     * @param context  context
     * @param timeOut  timeout values in millisecs
     * @param textView textview on which timeout has to be shown
     * @param color  color of the textview (Color.White , Color.parseColor("#3434FF"))
     */
    public SRCountDownTimer(Context context, long timeOut, TextView textView, int color)
    {
        this.context =context;
        this.timeout = timeOut;
        this.textView = textView;
        this.textColorPrimary = color;

    }

    /**
     * This function is called to start the count down timer
     */

    public void start_countdown_timer()
    {
        elapsed_time_otp=0;
        C_Minutes=(timeout/60000)-1;
        C_Seconds = 0;

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                try {
                    textView.setText("");
                    textView.setVisibility(View.VISIBLE);
                    textView.setTextColor(textColorPrimary);

                }catch (Exception e){}
            }
        });


        //Countdonw timer for OTP Validity
        countDownTimer = new CountDownTimer(timeout, 1000)
        {
            @Override
            public void onTick(long millisUntilFinished)
            {
                elapsed_time_otp++;
                C_Seconds =60-(elapsed_time_otp%60);  //GEt seconds
             //   Log.d("SYBLI_UPI","min "+C_Minutes+" Sec "+C_Seconds);
                if(C_Seconds==60)
                {
                    C_Seconds = 59;
                    C_Minutes--;
                }

                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        try {
                            if (C_Seconds < 10) {
                                textView.setText("" + C_Minutes + ":0" + C_Seconds);
                            } else {
                                textView.setText("" + C_Minutes + ":" + C_Seconds);
                            }
                        }catch(Exception e){}
                    }
                });

            }

            @Override
            public void onFinish()
            {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        try {
                            onTimerEvents.onCompletionTimer();
                            textView.setVisibility(View.VISIBLE);
                            textView.setTextColor(Color.RED);
                            textView.setText("00:00");
                            textView.setTextColor(Color.RED);
                        }catch (Exception e){}
                    }
                });

            }

        }.start();
    }

    /**
     * This function is called to stop the count down timer
     */
    public void stop_countdown_timer()
    {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                try {
                    countDownTimer.cancel();
                    textView.setText("");
                    textView.setVisibility(View.INVISIBLE);
                    textView.setTextColor(textColorPrimary);
                }catch(Exception e){}

            }
        });

    }

    /**
     * This method is used stop the count down timer without any UI
     * cahnge
     */
    public void stop(){
        countDownTimer.cancel();
    }

    /**
     * This interface provides method for the callback
     * on timer events
     */
    public interface onTimerEvents {
        void onCompletionTimer();
    }

    /**
     * This method is used to set the timer events callback interface
     * @param onTimerEvents
     */
    public void setCallBackInterface(onTimerEvents onTimerEvents){
        this.onTimerEvents = onTimerEvents;
    }
}
