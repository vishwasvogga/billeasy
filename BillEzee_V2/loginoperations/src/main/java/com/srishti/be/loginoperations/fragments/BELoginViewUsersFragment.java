package com.srishti.be.loginoperations.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srishti.be.loginoperations.R;
import com.srishti.be.loginoperations.activitys.BELoginMainActivity;
import com.srishti.be.loginoperations.adapters.BELoginRecycleViewLineDividerDecoration;
import com.srishti.be.loginoperations.adapters.BELoginViewUsersRecyclerAdapter;
import com.srishti.be.loginoperations.dboperations.BELoginDbOperations;
import com.srishti.be.utility.SRAlertBuilder;
import com.srishti.be.utility.SRCloseKeyboard;
import com.srishti.be.utility.SRDisplayMessage;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRGetScreenResolution;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.interfaces.SRActivityBridge;
import com.srishti.be.utility.interfaces.SRFragmentBridge;
import com.srishtiesdm.slogindb.entity.User;

import java.util.ArrayList;
import java.util.List;

import static com.srishti.be.loginoperations.constants.BELoginConstants.RESETTED_PASSWORD;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_ADMIN;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_OPERATOR;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_SUPERVISOR;
import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ROLE_KEY;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_DELETE;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_MODE;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_ONLY;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_RESET;

/**
 * This fragment is used to view/Delete the users
 */

public class BELoginViewUsersFragment extends Fragment implements SRFragmentBridge {
    private LinearLayout baseLayout=null;
    private Activity activity=null;
    private SRGetScreenResolution screenResolution = null;
    private SRGetResources resources=null;
    private int userRole=ROLE_SUPERVISOR;
    private int viewMode =0;
    private SRLog log = null;
    private SRDisplayMessage displayMessage=null;
    private BELoginDbOperations dbOperations=null;
    private SRActivityBridge srActivityBridge=null;
    private RecyclerView RV_users=null;
    private Button B_Back=null;
    private TextView T_MessageLine=null;
    private List<User> users=null;
    private SRFragmentTransactionHelper srFragmentTransactionHelper=null;
    private View.OnClickListener onRecycleViewItemClick=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        screenResolution = new SRGetScreenResolution(activity);
        baseLayout=(LinearLayout) activity.getLayoutInflater().inflate(R.layout.viewusers_layout,null);
        return baseLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        //Initialise the components
        initialise();
        //Add action listeners to various UI elements
        addActionListeners();

    }

    @Override
    public void onResume() {
        super.onResume();
        //Configure the various components
        configure();
    }

    /**
     * This method initialises all the UI elements and
     * class objects and variables
     */
    private void initialise(){
        //Get UI References
        RV_users = (RecyclerView) getViewById(R.id.RV_VU_usersDisplay);
        T_MessageLine=(TextView) getViewById(R.id.T_VU_messageLine);
        B_Back=(Button) getViewById(R.id.B_VU_Back);

        resources = SRGetResources.getInstance(activity);
        log = SRLog.getInstance();
        displayMessage =new SRDisplayMessage(activity);
        dbOperations = BELoginDbOperations.getInstance(activity);
        srActivityBridge = (SRActivityBridge)activity;
        srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(activity);
    }

    /**
     * This method is used get the views from id
     * @param id ID of the view
     * @return View
     */
    private View getViewById(int id){
        return activity.findViewById(id);
    }

    /**
     * This method is used to configure the
     * various elements
     */
    private void configure(){
        //Set recycle view layout
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false);
        RV_users.setLayoutManager(linearLayoutManager);
        RV_users.addItemDecoration(new BELoginRecycleViewLineDividerDecoration(activity));
        //GEt the shall delete and user role from input bundle
        Bundle configIn = getArguments();
        if(configIn!=null){
            try {
                userRole = configIn.getInt(USER_ROLE_KEY);
                viewMode = configIn.getInt(VIEW_MODE);
            }catch (Exception e){
                log.debug("No config receieved"+e.getMessage());
                e.printStackTrace();
            }
        }
        //Set the application constants , for interface between fragment and main activity
        BELoginMainActivity.fragmentBridge = this;
        //set message line
        srActivityBridge.function1(resources.getTextFromResource(R.string.view_User),null);
        //Set the text
        displayMessage.configure(true,3000,300,false,T_MessageLine);
        //Set the Recycle view
        Runnable setRecycleView = new Runnable() {
            @Override
            public void run() {
                setRecycleView(userRole);
            }
        };
        runOnThread(setRecycleView);
        //Configure message line
        if(viewMode==VIEW_DELETE) {
            srActivityBridge.function1(resources.getTextFromResource(R.string.delete_User), null);
        }else if(viewMode== VIEW_RESET){
            srActivityBridge.function1(resources.getTextFromResource(R.string.reset_User), null);
        }else{
            srActivityBridge.function1(resources.getTextFromResource(R.string.view_User), null);
        }
    }

    /**
     * This method will return all te users by user role
     * @param userRole user role
     */
    private void getAllUsers(int userRole){
        if(userRole==ROLE_SUPERVISOR){
            users= dbOperations.getAllUsersByUserrole(ROLE_OPERATOR);
        }else if(userRole==ROLE_ADMIN){
            users = new ArrayList<>();
            List<User> superVisors = dbOperations.getAllUsersByUserrole(ROLE_SUPERVISOR);
            List<User> operators = dbOperations.getAllUsersByUserrole(ROLE_OPERATOR);

            int lengthSupervisors = superVisors.size();
            int lengthOperators = operators.size();
            log.debug("No of supervisor and operator : "+(lengthOperators+lengthSupervisors));
            for(int i=0; i<lengthSupervisors+lengthOperators;i++){
                if(i<lengthSupervisors){
                    users.add(i,superVisors.get(i));
                }else{
                    users.add(i,operators.get(i-lengthSupervisors));
                }
            }
            log.debug("Length of users list (op+sup) : "+users.size());
        }else{
            users = new ArrayList<>();
        }
    }

    /**
     * This method will set all the users to recycleview
     * @param userRole user role
     */
    private void setRecycleView(int userRole){
        getAllUsers(userRole);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                BELoginViewUsersRecyclerAdapter recyclerAdapter = new BELoginViewUsersRecyclerAdapter(activity,
                        users,R.layout.viewusersrecycle_layout,onRecycleViewItemClick,viewMode);
                RV_users.setAdapter(recyclerAdapter);
            }
        };
        runOnUiThread(runnable);
    }

    /**
     * This method adds on click listeners to various UI elements
     */
    private void addActionListeners(){
        B_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back
                srFragmentTransactionHelper.popBackStack();
            }
        });

        onRecycleViewItemClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int itemClicked = RV_users.getChildAdapterPosition(v);
                //get the mode
                if(viewMode==VIEW_ONLY){
                    //Do nothing ifview only
                    return;
                }else if(viewMode==VIEW_DELETE){
                    //show alert dialog
                    //Then delete
                    String deleteTittle = resources.getTextFromResource(R.string.delete_User);
                    deleteTittle = deleteTittle +" "+ users.get(itemClicked).getUserID();
                    final SRAlertBuilder alertBuilder = new SRAlertBuilder(activity);
                    alertBuilder.createAlertBuilder(deleteTittle,
                            resources.getTextFromResource(R.string.doyouwanttodelete));
                    alertBuilder.addPositiveButton(resources.getTextFromResource(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                  //Call delete DB API
                                    Runnable deleteUser = new Runnable() {
                                        @Override
                                        public void run() {
                                            log.debug("Item no getting deleted "+itemClicked);
                                            boolean ret = dbOperations.deleteUserByUserId(users.get(itemClicked));
                                            if(ret){
                                                displayMessage.displayMessage(resources.getTextFromResource(R.string.success), Color.GREEN);
                                            }else{
                                                displayMessage.displayMessage(resources.getTextFromResource(R.string.failure), Color.RED);
                                            }
                                            //refresh list
                                            setRecycleView(userRole);
                                        }
                                    };
                                    runOnThread(deleteUser);
                                }
                            });

                    alertBuilder.addNegetiveButton(resources.getTextFromResource(R.string.no),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Do nothing
                                    alertBuilder.closeAlertDialog();
                                }
                            });
                    alertBuilder.showDialogBox();
                }else if(viewMode==VIEW_RESET){
                    //show alert dialog
                    //Then delete
                    String resetTittle = resources.getTextFromResource(R.string.reset_User);
                    resetTittle = resetTittle +" "+ users.get(itemClicked).getUserID();
                    //Then reset
                    final SRAlertBuilder alertBuilder = new SRAlertBuilder(activity);
                    String resetAlert =resources.getTextFromResource(R.string.resetAlert);
                      resetAlert = resetAlert+" "+RESETTED_PASSWORD+". "+resources.getTextFromResource(R.string.doyouwanttoReset);
                    alertBuilder.createAlertBuilder(resetTittle,resetAlert
                            );
                    alertBuilder.addPositiveButton(resources.getTextFromResource(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Call reset
                                    Runnable editUser = new Runnable() {
                                        @Override
                                        public void run() {
                                            log.debug("Item no getting resetted "+itemClicked);
                                            User user = users.get(itemClicked);
                                            //Set password as user ID
                                             user.setUserPassword(RESETTED_PASSWORD);
                                            int ret = dbOperations.editUser(user);
                                            if(ret==0){
                                                displayMessage.displayMessage(resources.getTextFromResource(R.string.success), Color.GREEN);
                                            }else{
                                                displayMessage.displayMessage(resources.getTextFromResource(R.string.failure), Color.RED);
                                            }
                                        }
                                    };
                                    runOnThread(editUser);
                                }
                            });

                    alertBuilder.addNegetiveButton(resources.getTextFromResource(R.string.no),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Do nothing
                                    alertBuilder.closeAlertDialog();
                                }
                            });
                    alertBuilder.showDialogBox();
                }
            }
        };

    }

    @Override
    public void onBackPressed(Object object1, Object object2) {

    }

    @Override
    public void onMenuSelected(int menuItemId, int menu) {

    }

    @Override
    public void onMoreMenuSelected() {

    }

    /**
     * This method is used to run on thread
     * @param runnable runnable which is to be run
     */
    public void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * This method is used to run the runnable on UI thread
     * @param runnable runnable which is to be run
     */
    public void runOnUiThread(Runnable runnable){
        activity.runOnUiThread(runnable);
    }

    @Override
    public void onStop() {
        new SRCloseKeyboard().closeTheKeyBoard(activity);
        super.onStop();
    }
}
