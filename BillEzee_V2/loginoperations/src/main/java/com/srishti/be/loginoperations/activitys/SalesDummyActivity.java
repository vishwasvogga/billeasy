package com.srishti.be.loginoperations.activitys;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srishti.be.utility.activity.SRFrameworkActivity;

/**
 * Created by Srishti-Admin on 07-10-2017.
 */

public class SalesDummyActivity extends SRFrameworkActivity {
    private LinearLayout linearLayout=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linearLayout = new LinearLayout(this);
        TextView textView = new TextView(this);
        textView.setText("This is sales page");
        textView.setTextColor(Color.BLUE);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(30);
        linearLayout.addView(textView);
        FrameLayout frameLayout =(FrameLayout) findViewById(com.srishti.be.utility.R.id.mainFragmentContainer);
        frameLayout.addView(linearLayout);
        setMessageLine((String)"Sales", Color.LTGRAY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        super.enableAllMenuItem();
        super.disableMenuItem(com.srishti.be.utility.R.id.TM_Home);
    }

    @Override
    public void onBackPressed() {
        //Disable back button
    }
}
