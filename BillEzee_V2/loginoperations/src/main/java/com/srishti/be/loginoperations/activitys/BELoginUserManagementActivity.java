package com.srishti.be.loginoperations.activitys;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.srishti.be.loginoperations.fragments.BELoginUserManagementMenuFragment;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.activity.SRFrameworkActivity;
import com.srishti.be.utility.interfaces.SRActivityBridge;

import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_MAIN_FRAGMENT_TAG;

/**
 * This activity is used to call the user management fucntion
 */

public class BELoginUserManagementActivity extends SRFrameworkActivity implements SRActivityBridge{

    private SRFragmentTransactionHelper srFragmentTransactionHelper=null;
    private BELoginUserManagementMenuFragment beLoginUserManagement=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialisation();
        configure();
    }


    /**
     * this method is used to initialise all the variables
     */
    private void initialisation(){
        srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(this);
        beLoginUserManagement = new BELoginUserManagementMenuFragment();
    }

    /**
     * This method is used to configure all the configarables
     */
    private void configure(){
        super.enableAllMenuItem();
        //Launch fragment
        launchUsermanagementFragment();
    }

    /**
     * This method is used to launch usermanagement fragment
     */
    private void launchUsermanagementFragment(){
        srFragmentTransactionHelper.ReplaceFragment(beLoginUserManagement, com.srishti.be.utility.R.id.mainFragmentContainer,SR_LOGIN_MAIN_FRAGMENT_TAG);
    }

    /**
     * This function is configured to use for setting message line
     * @param object1 String message
     * @param object2 null
     */
    @Override
    public void function1(Object object1, Object object2) {
        setMessageLine((String)object1, Color.LTGRAY);
    }

    @Override
    public void function2(Object object1, Object object2) {
        //Call the next activity here , after login.

    }

    @Override
    public void function3(Object object1, Object object2) {
        //set drawer Header
        super.setUserDetailsDrawer((String)object1,(String)object2);
    }

    @Override
    public void function4(Object object1, Object object2) {

    }

    @Override
    public void function5(Object object1, Object object2) {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
