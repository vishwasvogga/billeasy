package com.srishti.be.loginoperations.activitys;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.srishti.be.loginoperations.fragments.BELoginMyAccountFragment;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.activity.SRFrameworkActivity;
import com.srishti.be.utility.interfaces.SRActivityBridge;

import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MY_ACCOUNT_FRAGMENT_TAG;

/**
 * this activity takes to My account management
 */

public class BELoginMyAccountActivity extends SRFrameworkActivity implements SRActivityBridge {
    private SRFragmentTransactionHelper srFragmentTransactionHelper=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialisation();
        configure();
    }


    /**
     * this method is used to initialise all the variables
     */
    private void initialisation(){
        srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(this);
    }

    /**
     * This method is used to configure all the configarables
     */
    private void configure(){
        super.enableAllMenuItem();
        //Launch fragment
        launchUsermanagementFragment();
    }

    /**
     * This method is used to launch usermanagement fragment
     */
    private void launchUsermanagementFragment(){
        BELoginMyAccountFragment beLoginMyAccount = new BELoginMyAccountFragment();
        srFragmentTransactionHelper.ReplaceFragment(beLoginMyAccount,com.srishti.be.utility.R.id.mainFragmentContainer,SR_MY_ACCOUNT_FRAGMENT_TAG);
    }

    /**
     * This function is configured to use for setting message line
     * @param object1 String message
     * @param object2 null
     */
    @Override
    public void function1(Object object1, Object object2) {
        setMessageLine((String)object1, Color.LTGRAY);
    }

    @Override
    public void function2(Object object1, Object object2) {

    }

    @Override
    public void function3(Object object1, Object object2) {
        //set drawer Header
        super.setUserDetailsDrawer((String)object1,(String)object2);
    }

    @Override
    public void function4(Object object1, Object object2) {

    }

    @Override
    public void function5(Object object1, Object object2) {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
