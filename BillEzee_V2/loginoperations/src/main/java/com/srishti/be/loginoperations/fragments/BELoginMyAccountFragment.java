package com.srishti.be.loginoperations.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srishti.be.loginoperations.R;
import com.srishti.be.loginoperations.activitys.BELoginMainActivity;
import com.srishti.be.loginoperations.dboperations.BELoginDbOperations;
import com.srishti.be.utility.SRAlertBuilder;
import com.srishti.be.utility.SRCloseKeyboard;
import com.srishti.be.utility.SRCountDownTimer;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.SRProgressDialog;
import com.srishti.be.utility.activity.SRFrameworkApplication;
import com.srishti.be.utility.config.SRGetConfiguration;
import com.srishti.be.utility.interfaces.SRActivityBridge;
import com.srishti.be.utility.mail.SRSendMailRequestObject;
import com.srishti.be.utility.mail.SRSendMessageToMail;
import com.srishti.be.utility.phone.SRSendMessageToPhone;
import com.srishti.be.utility.sharedpreference.SRSharedPrefrence;
import com.srishtiesdm.slogindb.entity.User;

import static com.srishti.be.loginoperations.constants.BELoginConstants.SIGNUP_OTP_DURATION;
import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ID;
import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ROLE_KEY;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_PIN;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_UID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_BUTTON_TINT_ALPHA;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_CHANGE_CREDENTIALS_FRAGMENT_TAG;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_HOST;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_PORT;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_ID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_PASSWORD;

/**
 * This fragment is used give choice of my account options
 */

public class BELoginMyAccountFragment extends Fragment implements
        SRSendMessageToPhone.onOtpSendingResponce,
        SRCountDownTimer.onTimerEvents {
    private LinearLayout baseLayout = null;
    private Button B_ChangeCredential,B_DeleteAccount,B_Back=null;
    private Activity activity=null;
    private SRFragmentTransactionHelper srFragmentTransactionHelper=null;
    private BELoginDbOperations dbOperations=null;
    private SRGetResources resources=null;
    private SRLog log=null;
    private SRProgressDialog progressDialog=null;
    private SRAlertBuilder srAlertBuilder=null;
    private SRGetResources srGetResources=null;
    private SRCountDownTimer srCountDownTimer=null;
    private String otp="";
    private SRActivityBridge srActivityBridge=null;
    private SRGetConfiguration srGetConfiguration=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = (LinearLayout) inflater.inflate(R.layout.myaccount_layout,null);
        baseLayout =(LinearLayout) view;
        activity = getActivity();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();
        configure();
        onActionListener();
    }

    /**
     * Thismethod i s used to initilise various components
     */
    private void initialise(){
        B_ChangeCredential=(Button) baseLayout.findViewById(R.id.B_MA_ChangeCredentials);
        B_DeleteAccount=(Button) baseLayout.findViewById(R.id.B_MA_DeleteAccount);
        B_Back=(Button) baseLayout.findViewById(R.id.B_MA_back);
        srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(activity);
        dbOperations = BELoginDbOperations.getInstance(activity);
        resources = SRGetResources.getInstance(activity);
        log = SRLog.getInstance();
        srGetResources = SRGetResources.getInstance(activity);
        srGetConfiguration = SRGetConfiguration.getInstance(activity);
    }

    /**
     * This method is used to configure various configarable
     */
    private void configure(){
        srActivityBridge = (SRActivityBridge) activity;
        //set messageline
        srActivityBridge.function1(resources.getTextFromResource(R.string.myAccount),null);
        //config object
        boolean isChangeCredEnabled = srGetConfiguration.accessToSRCBELoginChangeCredentialsFragment;
        boolean isDeleteAccountEnabled = srGetConfiguration.accessToSRCBELoginDeleteAccountFlag;
        if(!isChangeCredEnabled){
            B_ChangeCredential.setEnabled(false);
            B_ChangeCredential.setAlpha(SR_BUTTON_TINT_ALPHA);
        }
        if(!isDeleteAccountEnabled){
            B_DeleteAccount.setEnabled(false);
            B_DeleteAccount.setAlpha(SR_BUTTON_TINT_ALPHA);
        }
    }

    /**
     * This method adds action listeners to the various UI elements
     */
    private void onActionListener(){
        B_ChangeCredential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BELoginChangeCredentialsFragment changeCredentialsFragment = new BELoginChangeCredentialsFragment();
                srFragmentTransactionHelper.ReplaceFragment(changeCredentialsFragment,SR_CHANGE_CREDENTIALS_FRAGMENT_TAG,
                        com.srishti.be.utility.R.id.mainFragmentContainer,SR_CHANGE_CREDENTIALS_FRAGMENT_TAG);

            }
        });

        B_DeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //alert dialog
                final SRAlertBuilder alertBuilder = new SRAlertBuilder(activity);
                String deleteAlert =resources.getTextFromResource(R.string.doyouwanttodelete);
                alertBuilder.createAlertBuilder(resources.getTextFromResource(R.string.Delete_Account),deleteAlert
                );
                alertBuilder.addPositiveButton(resources.getTextFromResource(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Send OTP
                                Runnable sendOtp = new Runnable() {
                                    @Override
                                    public void run() {
                                        sendOtp();
                                    }
                                };
                                runOnThread(sendOtp);
                            }
                        });

                alertBuilder.addNegetiveButton(resources.getTextFromResource(R.string.no),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Do nothing
                                alertBuilder.closeAlertDialog();
                            }
                        });
                alertBuilder.showDialogBox();
            }
        });

        B_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
    }

    /**
     * This method is used to run on thread
     * @param runnable runnable which is to be run
     */
    public void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * This method is used to run the runnable on UI thread
     * @param runnable runnable which is to be run
     */
    public void runOnUiThread(Runnable runnable){
        activity.runOnUiThread(runnable);
    }

    /**
     * This function fills the UI with exisiting user details from the DB
     */
    private void deleteUser(){
        Runnable getExistingUserDetails = new Runnable() {
            @Override
            public void run() {
                final User user=dbOperations.getUserByUserId(SRFrameworkApplication.SR_LOGIN_USERNAME);
                boolean ret = dbOperations.deleteUserByUserId(user);
                if(ret){
                    final SRAlertBuilder alertBuilder = new SRAlertBuilder(activity);
                    String deleteAlert =resources.getTextFromResource(R.string.success);
                    alertBuilder.createAlertBuilder(resources.getTextFromResource(R.string.Delete_Account),deleteAlert
                    );
                    alertBuilder.addPositiveButton(resources.getTextFromResource(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Clear all the details
                                    //set data in application
                                    SRFrameworkApplication.SR_LOGIN_USERNAME="";
                                    SRFrameworkApplication.SR_LOGIN_USERROLE=- 1;
                                    SRFrameworkApplication.SR_LOGIN_USER_PHONE="";
                                    SRFrameworkApplication.SR_LOGIN_USER_MAIL="";
                                    SRFrameworkApplication.SR_LOGIN_USER_PASSWORD="";
                                    //set data in shared preference
                                    SRSharedPrefrence srSharedPrefrence = SRSharedPrefrence.getInstance(activity);
                                    srSharedPrefrence.putString(USER_ID,"");
                                    srSharedPrefrence.putInt(USER_ROLE_KEY,-1);
                                   //Go to main Login activity
                                    //Clear all previous actiivyrt
                                    Intent goToLogin = new Intent(activity, BELoginMainActivity.class);
                                    goToLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(goToLogin);
                                }
                            });
                    alertBuilder.showDialogBox();
                }else{
                    //Failure alert
                    String deleteAlert =resources.getTextFromResource(R.string.failure);
                    alertBuilderWithOkButton(resources.getTextFromResource(R.string.Delete_Account),deleteAlert);
                }
            }
        };
        runOnThread(getExistingUserDetails);
    }

    /**
     * This method is called on completion of the countdown timer
     */
    @Override
    public void onCompletionTimer() {
        //on completion of the countdown timer close it
        alertBuilderWithOkButton("",srGetResources.getTextFromResource(R.string.otpExpired));
        if(srAlertBuilder != null){
            srAlertBuilder.closeAlertDialog();
        }
    }

    @Override
    public void onResponceOtp(int errorCode) {
        getDelay(100);
        progressDialog.dismissProgressDialog();
        getDelay(100);
        if(errorCode==200){
            Runnable afterOtpSuccess = new Runnable() {
                @Override
                public void run() {
                    log.debug("Success OTP Sending");
                    //it gets verified
                    srAlertBuilder = new SRAlertBuilder(activity);
                    srAlertBuilder.createAlertBuilder();
                    View view = activity.getLayoutInflater().inflate(R.layout.otpentrypopup,null);
                    srAlertBuilder.setCustomView(view);
                    srAlertBuilder.showDialogBox();

                    //Get pop up custom view references
                    TextView timeout=(TextView) view.findViewById(R.id.T_OEP_messageLine3);
                    final EditText eotp = (EditText) view.findViewById(R.id.E_OEP_Otp);
                    Button back = (Button) view.findViewById(R.id.B_OEP_Back);
                    Button ok = (Button) view.findViewById(R.id.B_OEP_Ok);
                    Button resend =(Button) view.findViewById(R.id.B_OEP_Resend);

                    //Create the countdown timer
                    srCountDownTimer = new SRCountDownTimer(activity,SIGNUP_OTP_DURATION,timeout, Color.BLACK);
                    srCountDownTimer.setCallBackInterface(BELoginMyAccountFragment.this);
                    srCountDownTimer.start_countdown_timer();

                    //Back button on click listeners
                    back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            srAlertBuilder.closeAlertDialog();
                        }
                    });
                    //Resend
                    resend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(srAlertBuilder!=null){
                                srAlertBuilder.closeAlertDialog();
                            }
                            sendOtp();
                        }
                    });
                    //Ok button
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(eotp.getText().toString().equals("")){
                                eotp.setHint(srGetResources.getTextFromResource(R.string.pleaseEnterSentOtp));
                                eotp.setHintTextColor(Color.RED);
                                return;
                            }
                            srCountDownTimer.stop_countdown_timer();
                            if (otp.equals(eotp.getText().toString())) {
                                //Delete User
                                srAlertBuilder.closeAlertDialog();
                                deleteUser();


                            }else{
                                //OTP Do not match
                                srAlertBuilder.closeAlertDialog();
                                alertBuilderWithOkButton("",srGetResources.getTextFromResource(R.string.otpDonotMatch));
                            }
                        }
                    });
                }
            };
            runOnUiThread(afterOtpSuccess);
        }else{
            log.debug("Failure error code"+errorCode);
            if(errorCode==-1){
                //No internet //error hitting server//network error
                alertBuilderWithOkButton("",srGetResources.getTextFromResource(R.string.networkError));
            }else {
                //error reaching server
                alertBuilderWithOkButton("",srGetResources.getTextFromResource(R.string.networkError));
            }
        }
    }

    /**
     * This method is used to introduce a delay in the current threda
     * @param delay delay in MS
     */
    private void getDelay(long delay){
        try{
            Thread.sleep(delay);
        }catch (Exception e){

        }
    }

    private void alertBuilderWithOkButton(String title,String message){
        srAlertBuilder = new SRAlertBuilder(activity);
        srAlertBuilder.createAlertBuilder(title,message);
        srAlertBuilder.addPositiveButton(srGetResources.getTextFromResource(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        srAlertBuilder.closeAlertDialog();
                    }
                });
        srAlertBuilder.showDialogBox();
    }


    /**
     * This method is used to reset the user password
     */
    private void sendOtp(){
        otp = new SRSendMessageToPhone(activity).getRandomOtp(6);
        SRLog.getInstance().debug("OTP" + otp);
        String smsMessage = "Use " + otp
                + " as the OTP for completing your BillEzee  Password Reset. Valid for 15 minutes only. Please do not share your OTP with others."
                + "&rtype=json";

        String mailMessage = "Use " + otp
                + " as the OTP for completing your BillEzee Password Reset. Valid for 15 minutes only. Please do not share your OTP with others.";

        smsMessage = smsMessage.replaceAll("\\n", "%0A");
        smsMessage = smsMessage.replaceAll("\\s", "%20");
        smsMessage = smsMessage.replaceAll(":", "%3A");
        smsMessage = smsMessage.replaceAll("\\u20B9", "Rs");

        String url = "http://billpay.aapna-seva.in/api/sms.php?uid=" + SR_ACHAARIYA_UID + "&pin=" + SR_ACHAARIYA_PIN
                + "&sender=SSESDM&route=5&tempid=2&mobile=" +  SRFrameworkApplication.SR_LOGIN_USER_PHONE + "&message=" + smsMessage + "&pushid=1";
        SRLog.getInstance().debug(url);
        progressDialog = new SRProgressDialog(activity);
        progressDialog.createProgressDialog(srGetResources.getTextFromResource(R.string.otp),
                srGetResources.getTextFromResource(R.string.pleasewait));
        SRSendMessageToPhone srSendMessageToPhone = new SRSendMessageToPhone(activity);
        srSendMessageToPhone.sendOtpThruAchaaria(url, BELoginMyAccountFragment.this);
        //wait for sending otp call back

        //Also send OTP to mail
        SRSendMessageToMail srSendMessageToMail = new SRSendMessageToMail(activity);
        SRSendMailRequestObject srSendMailRequestObject = new SRSendMailRequestObject();
        srSendMailRequestObject.mailHost = SR_MAIL_HOST;
        srSendMailRequestObject.mailPort = SR_MAIL_PORT;
        srSendMailRequestObject.message = mailMessage;
        srSendMailRequestObject.senderMailId = SR_SRISHTI_MAIL_ID;
        srSendMailRequestObject.senderMailIdPassword = SR_SRISHTI_MAIL_PASSWORD;
        srSendMailRequestObject.receiverMailId = SRFrameworkApplication.SR_LOGIN_USER_MAIL;
        srSendMailRequestObject.tittle = "OTP for billeasy password reset";
        //send mail
        srSendMessageToMail.sendMail(srSendMailRequestObject);
    }

    @Override
    public void onStop() {
        new SRCloseKeyboard().closeTheKeyBoard(activity);
        super.onStop();
    }
}
