package com.srishti.be.loginoperations.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.srishti.be.loginoperations.R;
import com.srishti.be.loginoperations.dboperations.BELoginDbOperations;
import com.srishti.be.utility.SRAlertBuilder;
import com.srishti.be.utility.SRCloseKeyboard;
import com.srishti.be.utility.SRCountDownTimer;
import com.srishti.be.utility.SRDisplayMessage;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRGetScreenResolution;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.SRProgressDialog;
import com.srishti.be.utility.activity.SRFrameworkApplication;
import com.srishti.be.utility.interfaces.SRActivityBridge;
import com.srishti.be.utility.mail.SRSendMailRequestObject;
import com.srishti.be.utility.mail.SRSendMessageToMail;
import com.srishti.be.utility.phone.SRSendMessageToPhone;
import com.srishtiesdm.slogindb.entity.User;

import static android.view.View.GONE;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_ADMIN;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_OPERATOR;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_SUPERVISOR;
import static com.srishti.be.loginoperations.constants.BELoginConstants.SIGNUP_OTP_DURATION;
import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ROLE_KEY;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_PIN;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_UID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_HOST;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_PORT;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_ID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_PASSWORD;

/**
 * This fragment is used to add a new user to the login table
 */

public class BELoginRegisterNewUserFragment extends Fragment implements  SRSendMessageToPhone.onOtpSendingResponce,
        SRCountDownTimer.onTimerEvents {
    private LinearLayout baseLayout=null;
    private Activity activity=null;
    private SRGetScreenResolution screenResolution = null;
    private ScrollView SRmainScroll=null;
    private TextView TUserRole,TMessage=null;
    private RadioButton RBSupervisor,RBOperator=null;
    private EditText EUserId,EPhonenumber,EMailID,EPassword,EConfirmPassword=null;
    private TextInputLayout TILUserId,TILPhonenumber,TILMailId,TILPassword,TILConfirmPassword=null;
    private Button BBack,BAdd;
    private SRGetResources resources=null;
    private int userRole=4;
    private SRLog log = null;
    private SRDisplayMessage displayMessage=null;
    private SRProgressDialog progressDialog=null;
    private BELoginDbOperations dbOperations=null;
    private SRCountDownTimer srCountDownTimer=null;
    private String otp="-1";
    private String userId,password,mobileNumber,mailId="";
    ;private SRAlertBuilder srAlertBuilder=null;
    private SRActivityBridge srActivityBridge=null;
    private int roleOfNewUser=4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        screenResolution = new SRGetScreenResolution(activity);
        baseLayout=(LinearLayout) activity.getLayoutInflater().inflate(R.layout.registernewuser_layout,null);
        return baseLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        //Initialise the components
        initialise();
        //Add action listeners to various UI elements
        addActionListeners();

    }

    @Override
    public void onResume() {
        super.onResume();
        //Configure the various components
        configure();
    }

    /**
     * This method initialises all the UI elements and
     * class objects and variables
     */
    private void initialise(){
        //Get UI References
        SRmainScroll = (ScrollView) getViewById(R.id.SV_RNU_scrollview1);
        TUserRole = (TextView) getViewById(R.id.T_RNU_userRole);
        RBSupervisor = (RadioButton) getViewById(R.id.RB_RNU_superVisor);
        RBOperator = (RadioButton) getViewById(R.id.RB_RNU_operator);
        EUserId = (EditText) getViewById(R.id.E_RNU_userId);
        TILUserId =(TextInputLayout) getViewById(R.id.TI_RNU_userId);
        EPhonenumber = (EditText) getViewById(R.id.E_RNU_PhoneNumber);
        TILPhonenumber=(TextInputLayout) getViewById(R.id.TI_RNU_phoneNumber);
        EMailID=(EditText)getViewById(R.id.E_RNU_mailId);
        TILMailId=(TextInputLayout)getViewById(R.id.TI_RNU_mailId);
        TMessage = (TextView)getViewById(R.id.T_RNU_messageLine);
        EPassword=(EditText)getViewById(R.id.E_RNU_password);
        TILPassword=(TextInputLayout)getViewById(R.id.TI_RNU_password);
        EConfirmPassword=(EditText)getViewById(R.id.E_RNU_confirmPassword);
        TILConfirmPassword=(TextInputLayout)getViewById(R.id.TI_RNU_confirmPassword);
        BAdd=(Button)getViewById(R.id.B_RNU_Add);
        BBack=(Button)getViewById(R.id.B_RNU_Back);
        resources = SRGetResources.getInstance(activity);
        log = SRLog.getInstance();
        displayMessage =new SRDisplayMessage(activity);
        dbOperations = BELoginDbOperations.getInstance(activity);
        srActivityBridge = (SRActivityBridge)activity;
    }

    /**
     * This method is used get the views from id
     * @param id ID of the view
     * @return View
     */
    private View getViewById(int id){
        return activity.findViewById(id);
    }

    /**
     * This method is used to configure the
     * various elements
     */
    private void configure(){
        //set message line
        srActivityBridge.function1(resources.getTextFromResource(R.string.adduser),null);
        //Set the text
        TUserRole.setText(resources.getTextFromResource(R.string.userrole));
        RBSupervisor.setText(resources.getTextFromResource(R.string.supervisor));
        RBOperator.setText(resources.getTextFromResource(R.string.operator));
        TILUserId.setHint(resources.getTextFromResource(R.string.userid));
        TILPhonenumber.setHint(resources.getTextFromResource(R.string.phonenumber));
        TILMailId.setHint(resources.getTextFromResource(R.string.mailId));
        TILPassword.setHint(resources.getTextFromResource(R.string.password));
        TILConfirmPassword.setHint(resources.getTextFromResource(R.string.confirmpassword));
        BAdd.setText(resources.getTextFromResource(R.string.add));
        BBack.setText(resources.getTextFromResource(R.string.back));
        displayMessage.configure(true,3000,300,false,TMessage);
        //get the bundle passed by caller
        Bundle configInput = getArguments();
        if(configInput!=null) {
            userRole = configInput.getInt(USER_ROLE_KEY);
        }
        if(userRole==1){
            RBOperator.setVisibility(GONE);
            RBSupervisor.setVisibility(GONE);
            LinearLayout LLRBS =(LinearLayout) getViewById(R.id.LL_RNU_userRole);
            LLRBS.setVisibility(GONE);
            TUserRole.setVisibility(GONE);
        }else if(userRole==2){
            RBSupervisor.setChecked(true);
            RBOperator.setChecked(false);
        }else if(userRole==3){
            RBSupervisor.setChecked(false);
            RBOperator.setChecked(true);
            RBOperator.setEnabled(false);
            RBSupervisor.setEnabled(false);
        }else if(userRole==4){
            RBSupervisor.setChecked(false);
            RBOperator.setChecked(false);
            RBOperator.setEnabled(false);
            RBSupervisor.setEnabled(false);
        }

        if(userRole == ROLE_ADMIN || userRole==ROLE_SUPERVISOR){
            //Do nothing
            //Set optional text
            String SEEmailIdText = TILMailId.getHint().toString();
            SEEmailIdText = SEEmailIdText+" "+resources.getTextFromResource(R.string.optional);
            TILMailId.setHint(SEEmailIdText);

            String SEPhonenumber = TILPhonenumber.getHint().toString();
            SEPhonenumber = SEPhonenumber+" "+resources.getTextFromResource(R.string.optional);
            TILPhonenumber.setHint(SEPhonenumber);
        }
    }

    /**
     * Add various action listeners to the UI elements
     */
    private void addActionListeners(){
        //Back button
        BBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back
                new SRFragmentTransactionHelper(activity).popBackStack();
            }
        });

        //Add button
        BAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Add the user to the login table
                Runnable addUser = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            fieldsCheck();
                        }catch (Exception e){
                            displayMessage.displayMessage(e.getMessage(),Color.RED);
                        }
                    }
                };
               runOnThread(addUser);
            }
        });

        //Radio button supervisor
        RBSupervisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RBOperator.setChecked(false);
            }
        });
        //Radio button operator
        RBOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RBSupervisor.setChecked(false);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * THis method checks all the necessary fields
     * @return true - SUccess validation else otherwise
     */
    private boolean fieldsCheck(){
        if(userRole==1){
            //if super user is creating the admin then role of user is admin
            //admin
            //No check for user role
            log.debug("user role is admin");
            roleOfNewUser=ROLE_ADMIN;

        }else {
            //Super visor or operator
            if (RBOperator.isChecked()) {
                roleOfNewUser = ROLE_OPERATOR;
                log.debug("user role is operator");
            } else if (RBSupervisor.isChecked()) {
                roleOfNewUser = ROLE_SUPERVISOR;
                log.debug("user role is supervisor");
            } else {
                //Throw error
                log.debug("user role is not defined");
                displayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseselectuserrole), Color.RED);
                return false;
            }
        }
            //Check user ID
            String userId= EUserId.getText().toString();
            //Convert to lower case
            userId = userId.toLowerCase();
            this.userId= userId;
            if(userId.equalsIgnoreCase("")){
                displayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseentervalidusername), Color.RED);
                return false;
            }else{
                //Check for that user name in DB
                User user = dbOperations.getUserByUserId(userId);
                if(user == null){
                    //no user present proceed
                }else{
                    displayMessage.displayMessage(resources.getTextFromResource(R.string.useridalreadypresent), Color.RED);
                    log.debug("User already present "+user.toString());
                    return false;
                }
            }
            //if yoy are adding admin then only check for phone and email
            String mobileNumber = this.mobileNumber = EPhonenumber.getText().toString();
            String mailId = this.mailId = EMailID.getText().toString();
            if(userRole==1) {
                //Check Mobile number

                if (mobileNumber.equalsIgnoreCase("") || (mobileNumber.length() < 10)) {
                    displayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseentervalidphonenumber), Color.RED);
                    return false;
                    //Below is the code to check duplicate phone number
                }/*else{
                //Check for that user name in DB
                User user = dbOperations.getUserByPhoneNumber(mobileNumber);
                if(user == null){
                    //no user present proceed
                }else{
                    displayMessage.displayMessage(resources.getTextFromResource(R.string.phonenumberalreadypresent), Color.RED);
                    return false;
                }
            }*/

                //Check Mail id

                if (mailId.equalsIgnoreCase("") || !(mailId.contains("@"))) {
                    displayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseentervalidemailid), Color.RED);
                    return false;
                    //Below is the code to check duplicate Mail ID
                }/*else{
                //Check for that user name in DB
                User user = dbOperations.getUserByMailId(mailId);
                if(user == null){
                    //no user present proceed
                }else{
                    displayMessage.displayMessage(resources.getTextFromResource(R.string.mailidalreadypresent), Color.RED);
                    return false;
                }
            }*/
            }

            //Check pasword field
            String pasword=this.password = EPassword.getText().toString();
            if(pasword.equalsIgnoreCase("") ){
                displayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseentervalidpassword), Color.RED);
                return false;
            }else{
               //check for confirm password
                String cpasword = EConfirmPassword.getText().toString();
                if(cpasword.equalsIgnoreCase("")){
                    displayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseconfirmpassword), Color.RED);
                    return false;
                }else
                if(!cpasword.equalsIgnoreCase(pasword)){
                    displayMessage.displayMessage(resources.getTextFromResource(R.string.passwordsdonotmatch), Color.RED);
                    return false;
                }else {
                    //Password confirmed
                    //If Admin
                    //send the OTP to specified phonenumber and wait till then
                    //if you are adding admin then OTP
                    if(userRole==ROLE_ADMIN-1) {
                        otp = new SRSendMessageToPhone(activity).getRandomOtp(6);
                        SRLog.getInstance().debug("OTP" + otp);
                        String smsMessage = "Use " + otp
                                + " as the OTP for completing your BillEzee Sign Up. Valid for 15 minutes only. Please do not share your OTP with others."
                                + "&rtype=json";
                        String mailMessage = "Use " + otp
                                + " as the OTP for completing your BillEzee Sign Up. Valid for 15 minutes only. Please do not share your OTP with others.";

                        smsMessage = smsMessage.replaceAll("\\n", "%0A");
                        smsMessage = smsMessage.replaceAll("\\s", "%20");
                        smsMessage = smsMessage.replaceAll(":", "%3A");
                        smsMessage = smsMessage.replaceAll("\\u20B9", "Rs");

                        String url = "http://billpay.aapna-seva.in/api/sms.php?uid=" + SR_ACHAARIYA_UID + "&pin=" + SR_ACHAARIYA_PIN
                                + "&sender=SSESDM&route=5&tempid=92254&mobile=" + mobileNumber + "&message=" + smsMessage + "&pushid=1";
                        log.debug("URL"+url);
                        progressDialog = new SRProgressDialog(activity);
                        progressDialog.createProgressDialog(resources.getTextFromResource(R.string.otp),
                                resources.getTextFromResource(R.string.pleasewait));
                        SRSendMessageToPhone srSendMessageToPhone = new SRSendMessageToPhone(activity);
                        srSendMessageToPhone.sendOtpThruAchaaria(url, this);
                        //wait for sending otp call back

                        //Also send OTP to mail
                        SRSendMessageToMail srSendMessageToMail = new SRSendMessageToMail(activity);
                        SRSendMailRequestObject srSendMailRequestObject = new SRSendMailRequestObject();
                        srSendMailRequestObject.mailHost = SR_MAIL_HOST;
                        srSendMailRequestObject.mailPort = SR_MAIL_PORT;
                        srSendMailRequestObject.message = mailMessage;
                        srSendMailRequestObject.senderMailId = SR_SRISHTI_MAIL_ID;
                        srSendMailRequestObject.senderMailIdPassword = SR_SRISHTI_MAIL_PASSWORD;
                        srSendMailRequestObject.receiverMailId = mailId;
                        srSendMailRequestObject.tittle = "OTP for billeasy sign up";
                        //send mail
                        srSendMessageToMail.sendMail(srSendMailRequestObject);
                    }else{
                        //add operator/Supervisor
                        if(addUserToDb()==0){
                            //Adding user success
                            displayMessage.displayMessage(resources.getTextFromResource(R.string.userAdded),Color.GREEN);
                            //Pop back stack after delay 2seconds
                            popBackStackWithDelay(1000);

                        }else{
                            //Adding user success
                            displayMessage.displayMessage(resources.getTextFromResource(R.string.userNotAdded),Color.RED);
                        }
                    }
                }
            }
        return false;
    }

    /**
     * This method is used to run on thread
     * @param runnable runnable which is to be run
     */
    public void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * This method is used to run the runnable on UI thread
     * @param runnable runnable which is to be run
     */
    public void runOnUiThread(Runnable runnable){
        activity.runOnUiThread(runnable);
    }


    @Override
    public void onResponceOtp(int errorCode) {
        getDelay(100);
        progressDialog.dismissProgressDialog();
        getDelay(100);
        if(errorCode==200){
            Runnable afterOtpSuccess = new Runnable() {
                @Override
                public void run() {
                    log.debug("Success OTP Sending");
                    //it gets verified
                     srAlertBuilder = new SRAlertBuilder(activity);
                    srAlertBuilder.createAlertBuilder();
                    View view = activity.getLayoutInflater().inflate(R.layout.otpentrypopup,null);
                    srAlertBuilder.setCustomView(view);
                    srAlertBuilder.showDialogBox();

                    //Get pop up custom view references
                    TextView timeout=(TextView) view.findViewById(R.id.T_OEP_messageLine3);
                    final EditText eotp = (EditText) view.findViewById(R.id.E_OEP_Otp);
                    Button back = (Button) view.findViewById(R.id.B_OEP_Back);
                    Button ok = (Button) view.findViewById(R.id.B_OEP_Ok);

                    //Create the countdown timer
                    srCountDownTimer = new SRCountDownTimer(activity,SIGNUP_OTP_DURATION,timeout,Color.BLACK);
                    srCountDownTimer.setCallBackInterface(BELoginRegisterNewUserFragment.this);
                    srCountDownTimer.start_countdown_timer();

                    //Back button on click listeners
                    back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            srAlertBuilder.closeAlertDialog();
                        }
                    });
                    //Ok button
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(eotp.getText().toString().equals("")){
                                eotp.setHint(resources.getTextFromResource(R.string.pleaseEnterSentOtp));
                                eotp.setHintTextColor(Color.RED);
                                return;
                            }
                            srCountDownTimer.stop_countdown_timer();
                            if (otp.equals(eotp.getText().toString())) {
                                //Add user to DB
                                if(addUserToDb()==0){
                                    //Adding user success
                                    displayMessage.displayMessage(resources.getTextFromResource(R.string.userAdded),Color.GREEN);

                                }else{
                                    //Adding user failed
                                    displayMessage.displayMessage(resources.getTextFromResource(R.string.userNotAdded),Color.RED);
                                }
                                srAlertBuilder.closeAlertDialog();
                                SRFrameworkApplication.SR_LOGIN_USERNAME = userId;
                                SRFrameworkApplication.SR_LOGIN_USER_PASSWORD=password;
                                popBackStackWithDelay(1000);
                            }else{
                                //OTP Do not match
                                srAlertBuilder.closeAlertDialog();
                                displayMessage.displayMessage(resources.getTextFromResource(R.string.otpDonotMatch),Color.RED);
                            }
                        }
                    });
                }
            };
            runOnUiThread(afterOtpSuccess);
        }else{
            log.debug("Failure error code"+errorCode);
            if(errorCode==-1){
                //No internet //error hitting server//network error
                displayMessage.displayMessage(resources.getTextFromResource(R.string.networkError),Color.RED);
            }else {
                //error reaching server
                displayMessage.displayMessage(resources.getTextFromResource(R.string.networkError)+errorCode,Color.RED);
            }
        }
    }

    /**
     * This method is used to introduce a delay in the current threda
     * @param delay delay in MS
     */
    private void getDelay(long delay){
        try{
            Thread.sleep(delay);
        }catch (Exception e){

        }
    }

    /**
     * This method adds the used to the DB
     */
    private int addUserToDb(){
        //Add the user
        String encryptedPassword = "66736!dfshRe2";
        try {
            encryptedPassword = dbOperations.encryptPassword(password);
            log.debug("Encrypted password" + encryptedPassword);
        } catch (Exception e) {
            log.debug("error in enrcyption mechanism" + e);
            e.printStackTrace();
        }
        User user = new User();
        user.setUserID(userId);
        user.setUserPhone(mobileNumber);
        user.setUserEmailID(mailId);
        user.setUserPassword(encryptedPassword);
        user.setUserRole(roleOfNewUser);
        return dbOperations.addUser(user);
    }

    /**
     * This method is called on completion of the countdown timer
     */
    @Override
    public void onCompletionTimer() {
        //on completion of the countdown timer close it
        displayMessage.displayMessage(resources.getTextFromResource(R.string.otpExpired),Color.RED);
        if(srAlertBuilder != null){
            srAlertBuilder.closeAlertDialog();
        }
    }

    /**
     * This method is used to pop back stack on UI thread
     */
    private void popBackStack(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                boolean state=SRFragmentTransactionHelper.getInstance(activity).popBackStack();
            }
        };
        runOnUiThread(runnable);
    }

    /**
     * this method is used to pop back stack after a delay
     * @param delay
     */
    private void popBackStackWithDelay(final long delay){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try{
                    popBackStack();
                }catch (Exception e){
                    log.debug("Error in popping"+e.getMessage());
                    e.printStackTrace();
                }
            }
        },delay);
    }

    @Override
    public void onStop() {
        new SRCloseKeyboard().closeTheKeyBoard(activity);
        super.onStop();
    }


}
