package com.srishti.be.loginoperations.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srishti.be.loginoperations.R;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRLog;
import com.srishtiesdm.slogindb.entity.User;

import java.util.List;

/**
 * This adapter is used for the recycle views
 * product list
 */

public class BELoginViewUsersRecyclerAdapter extends RecyclerView.Adapter {

    private Context context=null;
    private List<User> usersList =null;
    private int layoutResourceId=0;
    private MyViewHolder vh=null;
    private View.OnClickListener onClickListener=null;
    private SRLog srLog=null;
    private int operation=0;

    /**
     * This is the default constructor of this adapter
     * @param context context
     * @param users users list
     * @param layoutResourceId custom layout of the recycle view
     * @param onProductClickListener onclick listener of the item of the list
     * @param operation 0-view only,1-delete,2-reset
     */
    public BELoginViewUsersRecyclerAdapter(Context context, List<User> users,
                                           int layoutResourceId, @Nullable View.OnClickListener onProductClickListener,int operation) {
        super();
        this.context =context;
        this.usersList = users;
        this.layoutResourceId = layoutResourceId;
        this.onClickListener=onProductClickListener;
        srLog =SRLog.getInstance();
        this.operation = operation;
        srLog.debug("constructor BELoginViewUsersRecyclerAdapter");
    }


    @Override
    public int getItemCount() {
        int itemsSize= usersList.size();
        srLog.debug("Items size"+itemsSize);
        return itemsSize;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        srLog.debug("oncreate view holder");
        View v = LayoutInflater.from(context).inflate(layoutResourceId, null, false);
        vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        srLog.debug("onbinding view"+position);
        User user = usersList.get(position);

        //Make user id field 50 character long by appending space
        //Just to counteract the bug in androi which makes the view wrap content
        //even if match parent is applied
        int lengthUserId = user.getUserID().length();
        String temp= user.getUserID();
        //append space if to make it 50 char wide
        for (int i=0; i<50-lengthUserId;i++ ){
            temp=temp+" ";
        }

        //Set the user to view
        vh.userId.setText(temp);
        vh.Phonenumber.setText(user.getUserPhone());
        vh.mailId.setText(user.getUserEmailID());
        vh.userrole.setText(getUserRoleFromCode(user.getUserRole()));

        //hide phone and email field if emoty
        SRGetResources resources = SRGetResources.getInstance(context);
        if(user.getUserPhone().equals("")){
            vh.Phonenumber.setText(resources.getTextFromResource(R.string.notAvailable));
        }
        if(user.getUserEmailID().equals("")){
            vh.mailId.setText(resources.getTextFromResource(R.string.notAvailable));
        }

        //check the operation
        //View only
        if(operation==0){
            vh.opearationIcon.setImageResource(R.drawable.ic_done_black_36dp);
            //delete
        }else if(operation==1){
            vh.opearationIcon.setImageResource(R.drawable.ic_delete_black_36dp);
            //Reset
        }else if(operation==2){
            vh.opearationIcon.setImageResource(R.drawable.ic_settings_backup_restore_black_36dp);
        }

        //Set on click listener
        if(onClickListener!=null) {
            vh.baselayout.setOnClickListener(this.onClickListener);
        }
    }

    /**
     * Inner class view holder which extends recycle view view holder
     */
    private class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userId,Phonenumber,mailId,userrole=null;
        public ImageView opearationIcon =null;
        public LinearLayout baselayout=null;
        // init the item view's
        public MyViewHolder(View itemView) {
            super(itemView);
            //get the reference
            userId =(TextView) itemView.findViewById(R.id.T_VURV_userId);
            Phonenumber=(TextView) itemView.findViewById(R.id.T_VURV_phonenumber);
            mailId=(TextView) itemView.findViewById(R.id.T_VURV_mailId);
            userrole=(TextView) itemView.findViewById(R.id.T_VURV_role);
            opearationIcon = (ImageView) itemView.findViewById(R.id.I_VURV_OperationIcon);
            baselayout=(LinearLayout) itemView.findViewById(R.id.LL_VURV_baseLayout);
        }
    }

    /**
     * THis method returns the user role string from user role int
     * @param userrole integer code
     * @return role in string
     */
    private String getUserRoleFromCode(int userrole){
        SRGetResources resources = SRGetResources.getInstance(context);
        switch (userrole){
            case 1: return resources.getTextFromResource(R.string.super_admin);
            case 2: return resources.getTextFromResource(R.string.admin);
            case 3: return resources.getTextFromResource(R.string.supervisor);
            case 4: return resources.getTextFromResource(R.string.operator);
        }
        return "";
    }
}
