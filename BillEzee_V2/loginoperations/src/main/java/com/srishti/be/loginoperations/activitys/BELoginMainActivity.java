package com.srishti.be.loginoperations.activitys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.widget.Toast;

import com.srishti.be.loginoperations.R;
import com.srishti.be.loginoperations.fragments.BELoginMainPageFragment;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.activity.SRFrameworkActivity;
import com.srishti.be.utility.activity.SRFrameworkApplication;
import com.srishti.be.utility.config.SRGetConfiguration;
import com.srishti.be.utility.constants.SRApplicationConstants;
import com.srishti.be.utility.interfaces.SRActivityBridge;
import com.srishti.be.utility.interfaces.SRFragmentBridge;

import static com.srishti.be.utility.constants.SRApplicationConstants.SR_HOME_ACTIVITY_NAME;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_HOME_ACTIVITY_PACKAGE;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_INACTIVITY_AUTOLOGOUT_TIME_MS;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_MAIN_FRAGMENT_TAG;

/**
 * This is the main activity and entry point of the application
 */

public class BELoginMainActivity extends SRFrameworkActivity implements SRActivityBridge {
    private SRFragmentTransactionHelper srFragmentTransactionHelper=null;
    public static SRFragmentBridge fragmentBridge=null;
    private BELoginMainPageFragment beLoginMainPageFragment =null;
    private Activity activity=null;
    private SRGetConfiguration srGetConfiguration=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialisation();
        configuration();
        launchFirstFragment();
        loadConfigFromCsv();
    }

    /**
     * This method initialises all the UI and the class objects
     */
    private void initialisation(){
        srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(this);
        beLoginMainPageFragment = new BELoginMainPageFragment();
        activity = this;
    }

    /**
     * This method is used to configure all the configurables
     */
    private void configuration(){
        //Disbale drawer
        super.IS_DRAWER_ENABLED=false;
    }


    /**
     * This method is used to launch the fragment
     */
    private void launchFirstFragment(){
        srFragmentTransactionHelper.ReplaceFragment(beLoginMainPageFragment,SR_LOGIN_MAIN_FRAGMENT_TAG, com.srishti.be.utility.R.id.mainFragmentContainer,SR_LOGIN_MAIN_FRAGMENT_TAG);
    }

    @Override
    public void onBackPressed() {
        //static variable from application constants
        if(fragmentBridge !=null){
            try {
                fragmentBridge.onBackPressed(null, null);
            }catch (Exception e){
                super.onBackPressed();
            }
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        super.enableAllMenuItem();
        //Tint all the menu items in toolbar
        super.disableAllMenuItem();
    }

    /**
     * This function is configured to use for setting message line
     * @param object1 String message
     * @param object2 null
     */
    @Override
    public void function1(Object object1, Object object2) {
        setMessageLine((String)object1, Color.LTGRAY);
    }

    @Override
    public void function2(Object object1, Object object2) {
        //Start the auto logout timer
        super.startAutoLogoutTimer(SR_INACTIVITY_AUTOLOGOUT_TIME_MS);
        //set and get the user role access Code
        srGetConfiguration.setSRCUserAccess(SRFrameworkApplication.SR_LOGIN_USERROLE);
        //check the configaration status
        Runnable showToast = new Runnable() {
            @Override
            public void run() {
                if(!(srGetConfiguration.IS_CONFIG_SUCCESS)){
                    Toast toast = Toast.makeText(activity, R.string.configFailAlert,Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }
            }
        };
        runOnUiThread(showToast);
        //Call the next activity here , after login.
        Intent entryActivity = new Intent();
        entryActivity.setClassName(activity,SR_HOME_ACTIVITY_NAME);
        entryActivity.setPackage(SR_HOME_ACTIVITY_PACKAGE);
        startActivity(entryActivity);
    }

    @Override
    public void function3(Object object1, Object object2) {
        //set drawer Header
        super.setUserDetailsDrawer((String)object1,(String)object2);
    }

    @Override
    public void function4(Object object1, Object object2) {

    }

    @Override
    public void function5(Object object1, Object object2) {

    }

    /**
     * This method is used to load the config from the csv in Assets
     */
    private void loadConfigFromCsv(){
        Runnable loadConfig = new Runnable() {
            @Override
            public void run() {
                srGetConfiguration=SRGetConfiguration.getInstance(activity);
                final boolean ret = srGetConfiguration.loadConfigFromAssets();
                //check the configaration status
                Runnable showToast = new Runnable() {
                    @Override
                    public void run() {
                        if(!(ret && srGetConfiguration.IS_CONFIG_SUCCESS)){
                            Toast toast = Toast.makeText(activity, R.string.configFailAlert,Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,0,0);
                            toast.show();
                        }else{
                            //Configure the values
                            SRApplicationConstants.SR_MAIL_HOST = srGetConfiguration.mailHost;
                            SRApplicationConstants.SR_MAIL_PORT = srGetConfiguration.mailPort;
                            SRApplicationConstants.SR_INACTIVITY_AUTOLOGOUT_TIME_MS=srGetConfiguration.autoLogoutTimeout;
                            SRApplicationConstants.SR_NETWORK_TIMEOUT_MS=(int)srGetConfiguration.networkTimeout;
                        }
                    }
                };
                runOnUiThread(showToast);
            }
        };
        runOnThread(loadConfig);
    }

    /**
     * This method is used to run on thread
     * @param runnable runnable which is to be run
     */
    public void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }
}
