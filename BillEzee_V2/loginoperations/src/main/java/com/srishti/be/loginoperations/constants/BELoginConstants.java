package com.srishti.be.loginoperations.constants;

/**
 * This class holds the constants corresponding to Login module
 */

public class BELoginConstants {
    public static final String USER_ROLE_KEY="userrolekey";
    /**
     * While changing the roles please change accordingly in
     * SRGetConfiguration in the SR Framework
     */
    public static final int ROLE_SUPERVISOR=3;
    public static final int ROLE_OPERATOR=4;
    public static final int ROLE_ADMIN=2;
    public static final int MAX_NO_OF_ADMINS=3;
    public static final String VIEW_MODE ="view fragment mode";
    public static final int VIEW_ONLY=0;
    public static final int VIEW_DELETE=1;
    public static final int VIEW_RESET =2;

    public static final long SIGNUP_OTP_DURATION=900000;

    public static final String USER_ID="userid";
    public static final String USER_PASSWORD="password";
    public static final String USER_PHONE="userphone";
    public static final String USER_MAIL="usermail";
    public static final String RESETTED_PASSWORD="12345";

}
