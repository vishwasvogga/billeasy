package com.srishti.be.loginoperations.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srishti.be.loginoperations.R;
import com.srishti.be.loginoperations.activitys.BELoginMainActivity;
import com.srishti.be.loginoperations.dboperations.BELoginDbOperations;
import com.srishti.be.utility.SRAlertBuilder;
import com.srishti.be.utility.SRCloseKeyboard;
import com.srishti.be.utility.SRCountDownTimer;
import com.srishti.be.utility.SRDisplayMessage;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.SRProgressDialog;
import com.srishti.be.utility.activity.SRFrameworkApplication;
import com.srishti.be.utility.interfaces.SRActivityBridge;
import com.srishti.be.utility.interfaces.SRFragmentBridge;
import com.srishti.be.utility.mail.SRSendMailRequestObject;
import com.srishti.be.utility.mail.SRSendMessageToMail;
import com.srishti.be.utility.phone.SRSendMessageToPhone;
import com.srishti.be.utility.sharedpreference.SRSharedPrefrence;
import com.srishtiesdm.slogindb.entity.User;

import static com.srishti.be.loginoperations.constants.BELoginConstants.MAX_NO_OF_ADMINS;
import static com.srishti.be.loginoperations.constants.BELoginConstants.RESETTED_PASSWORD;
import static com.srishti.be.loginoperations.constants.BELoginConstants.SIGNUP_OTP_DURATION;
import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ID;
import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_PASSWORD;
import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ROLE_KEY;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_PIN;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_UID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_ADD_NEW_USER_FRAGMENT_TAG;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_HOST;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_PORT;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_ID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_PASSWORD;

/**
 * Created by Srishti-Admin on 06-10-2017.
 */

public class BELoginMainPageFragment extends Fragment implements SRFragmentBridge,
        SRSendMessageToPhone.onOtpSendingResponce,
        SRCountDownTimer.onTimerEvents{
    private Activity activity=null;
    private LinearLayout baseLayout=null;
    private TextView T_MessageLine,T_ForgotPassword,T_AddAdmin=null;
    private TextInputLayout TIL_userId,TIL_password=null;
    private EditText E_userId,E_password=null;
    private Button B_login=null;
    private BELoginDbOperations beLoginDbOperations=null;
    private SRGetResources resources=null;
    private int noOfAdmins=-1;
    private SRDisplayMessage srDisplayMessage=null;
    private SRActivityBridge srActivityBridge=null;
    private String otp="";
    private SRProgressDialog progressDialog=null;
    private SRGetResources srGetResources=null;
    private SRLog log=null;
    private SRAlertBuilder srAlertBuilder=null;
    private SRCountDownTimer srCountDownTimer=null;
    private User user=new User();
    private BELoginDbOperations dbOperations=null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        baseLayout=(LinearLayout) activity.getLayoutInflater().inflate(R.layout.loginmainpage_layout,null);
        SRLog.getInstance().debug("On create view login main frag");
        return baseLayout;
    }

    /**
     * Our code starts executing from here
     */
    @Override
    public void onStart() {
        super.onStart();
        initialise();
        configure();
        addOnClickListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This method is used to initialise all the UI elements
     * and class objects
     */
    private void initialise(){
        SRLog.getInstance().debug("initalising");
        T_MessageLine = (TextView) getViewById(R.id.T_LMP_messageLine);
        T_AddAdmin = (TextView) getViewById(R.id.T_LMP_addAdmin);
        T_ForgotPassword = (TextView) getViewById(R.id.T_LMP_forgotPassword);
        TIL_userId=(TextInputLayout) getViewById(R.id.TI_LMP_userId);
        TIL_password=(TextInputLayout)getViewById(R.id.TI_LMP_password);
        E_userId=(EditText)getViewById(R.id.E_LMP_userId);
        E_password=(EditText)getViewById(R.id.E_LMP_password);
        B_login=(Button)getViewById(R.id.B_LMP_Login);
        beLoginDbOperations = BELoginDbOperations.getInstance(activity);
        resources = SRGetResources.getInstance(activity);
        srDisplayMessage = new SRDisplayMessage(activity);
        srActivityBridge = (SRActivityBridge) activity;
        srGetResources = SRGetResources.getInstance(activity);
        log = SRLog.getInstance();
        dbOperations =BELoginDbOperations.getInstance(activity);
    }

    /**
     * This method is used to configure all the configarables
     */
    private void configure(){
        log.debug("configuring");
        //Set the message line
        srActivityBridge.function1(resources.getTextFromResource(R.string.login),null);
        //Set the application constants , for interface between fragment and main activity
        BELoginMainActivity.fragmentBridge = this;
        srDisplayMessage.configure(true,3000,300,false,T_MessageLine);
        T_MessageLine.setText(resources.getTextFromResource(R.string.login));
        TIL_password.setHint(resources.getTextFromResource(R.string.password));
        TIL_userId.setHint(resources.getTextFromResource(R.string.userId));
        T_AddAdmin.setText(resources.getTextFromResource(R.string.addAdmin));
        T_ForgotPassword.setText(resources.getTextFromResource(R.string.forgotPassword));
        //Get the user id and password from the config(Bundle argument) input and set edit text
        Bundle configIn=getArguments();
        if(configIn!=null){
            String userName = configIn.getString(USER_ID);
            String password = configIn.getString(USER_PASSWORD);
            if(userName!=null && !userName.equals("")){
                E_userId.setText(userName);
            }
            if(password!=null&&!password.equals("")){
                E_password.setText(password);
            }
        }
        //Get the userid  from Application class
        if(SRFrameworkApplication.SR_LOGIN_USERNAME!=null && !SRFrameworkApplication.SR_LOGIN_USERNAME.equals("")){
            E_userId.setText(SRFrameworkApplication.SR_LOGIN_USERNAME);
        }
        //Get user id from shared preference
        SRSharedPrefrence srSharedPrefrence = SRSharedPrefrence.getInstance(activity);
        String userID = srSharedPrefrence.getString(USER_ID,"");
        if(userID !=null && !userID.equals("")){
            E_userId.setText(userID);
        }
        //get number of admins
        //and configure add admin textview
        Runnable getAllUsers = new Runnable() {
            @Override
            public void run() {
               noOfAdmins= getNumberOfAdmins();
                if(noOfAdmins==0){
                    redirectToAddUserPage();
                }else if(noOfAdmins>= MAX_NO_OF_ADMINS){
                    //Hide add admin
                    SRDisplayMessage message = new SRDisplayMessage(activity);
                    message.configure(false,0,0,false,T_AddAdmin);
                    int color = T_AddAdmin.getCurrentTextColor();
                    message.setVisibility(View.INVISIBLE);
                }else if(noOfAdmins< MAX_NO_OF_ADMINS){
                    //Display the number of admins left
                    SRDisplayMessage message = new SRDisplayMessage(activity);
                    message.configure(false,0,0,false,T_AddAdmin);
                    int color = T_AddAdmin.getCurrentTextColor();
                    message.displayMessage(resources.getTextFromResource(R.string.addAdmin)+" "+
                            (MAX_NO_OF_ADMINS-noOfAdmins)+" "+resources.getTextFromResource(R.string.left)+"!",color);
                }
            }
        };
        runOnThread(getAllUsers);
        //Clear password field
        E_password.setText("");
        //set Message line color to blue
        T_MessageLine.setText(resources.getTextFromResource(R.string.login));
        T_MessageLine.setTextColor(resources.getColorFromResource(R.color.be_black));
        SRLog.getInstance().debug("config finish");
    }

    /**
     * This method will redirect to add users page
     */
    private void redirectToAddUserPage(){
        //Go to add users page
        Runnable goToAddUser = new Runnable() {
            @Override
            public void run() {
                final SRFragmentTransactionHelper srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(activity);
                BELoginRegisterNewUserFragment registerNewUser = new BELoginRegisterNewUserFragment();
                Bundle configIn = new Bundle();
                configIn.putInt(USER_ROLE_KEY,1); //1-Super admin creating an admin
                registerNewUser.setArguments(configIn);
                srFragmentTransactionHelper.ReplaceFragment(registerNewUser,SR_LOGIN_ADD_NEW_USER_FRAGMENT_TAG,R.id.mainFragmentContainer,SR_LOGIN_ADD_NEW_USER_FRAGMENT_TAG);
            }
        };
        runOnUiThread(goToAddUser);
    }

    /**
     * This method adds onclick listeners
     * to various UI elements
     */
    private void addOnClickListeners(){
        //Add Admin
        T_AddAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToAddUserPage();
            }
        });

        //Login button
        B_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable loginCheck = new Runnable() {
                    @Override
                    public void run() {
                        loginCheck();
                    }
                };
                runOnThread(loginCheck);

            }});

        T_ForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Runnable resetPassword = new Runnable() {
                    @Override
                    public void run() {
                        checkUserAndRetriveData();
                    }
                };
                runOnThread(resetPassword);
            }
        });
    }

    /**
     * This method is used to check the login credential an redirect accordingly
     */
    private void loginCheck(){
        String userId= E_userId.getText().toString();
        //Convert to lower case
        userId = userId.toLowerCase();
        String password = E_password.getText().toString();
        if(userId.equals("")){
            srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseentervalidusername), Color.RED);
            return;
        }else
        if(password.equals("")){
            srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseentervalidpassword), Color.RED);
            return;
        }else{
            user = beLoginDbOperations.getUserByUserId(userId);
     /*   //To hold the user details
        User user=new User();
        //Check if phone number
        if(userId.length()==10){
            user = beLoginDbOperations.getUserByPhoneNumber(userId);
        }
        //below is the code to get the user from phine and mail ID
        //Check if email
        else if(userId.contains("@")){
            user = beLoginDbOperations.getUserByMailId(userId);
        }else {
            //load user by user id*/

        }

        if(user == null){
            srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.userNotPresent), Color.RED);
            return;
        }else{
            //User is present check password
            SRLog.getInstance().debug("User entered password "+password);
            //get the password from user object
            String passwordFromDb ="";
            try {
                passwordFromDb = beLoginDbOperations.decryptPassword(user.getUserPassword());
                SRLog.getInstance().debug("Decrypted password "+passwordFromDb);
            }catch (Exception e){
                SRLog.getInstance().debug("Password decryption failed"+e.getMessage());
                e.printStackTrace();
            }
            if(password.equals(passwordFromDb)){
                //PAssword match
                //Show success
                //Or redirect
                srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.loginSuccess), Color.GREEN);
                //set data in application
                SRFrameworkApplication.SR_LOGIN_USERNAME=user.getUserID();
                SRFrameworkApplication.SR_LOGIN_USERROLE=user.getUserRole();
                SRFrameworkApplication.SR_LOGIN_USER_PHONE=user.getUserPhone();
                SRFrameworkApplication.SR_LOGIN_USER_MAIL=user.getUserEmailID();
                SRFrameworkApplication.SR_LOGIN_USER_PASSWORD=user.getUserPassword();
                //set data in shared preference
                SRSharedPrefrence srSharedPrefrence = SRSharedPrefrence.getInstance(activity);
                srSharedPrefrence.putString(USER_ID,SRFrameworkApplication.SR_LOGIN_USERNAME);
                srSharedPrefrence.putInt(USER_ROLE_KEY,SRFrameworkApplication.SR_LOGIN_USERROLE);
                //set user and role in drawer
                srActivityBridge.function3(SRFrameworkApplication.SR_LOGIN_USERNAME,getUserRoleFromCode(SRFrameworkApplication.SR_LOGIN_USERROLE));
                //Call the interface function in the BELoginMainActivity
                srActivityBridge.function2(null,null);
            }else{
                //Show failure
                srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.invalidPassword), Color.RED);
            }

        }
    }

    /**
     * THis method returns the user role string from user role int
     * @param userrole integer code
     * @return role in string
     */
    private String getUserRoleFromCode(int userrole){
        switch (userrole){
            case 1: return resources.getTextFromResource(R.string.super_admin);
            case 2: return resources.getTextFromResource(R.string.admin);
            case 3: return resources.getTextFromResource(R.string.supervisor);
            case 4: return resources.getTextFromResource(R.string.operator);
        }
        return "";
    }

    /**
     * This method is used get the views from id
     * @param id ID of the view
     * @return View
     */
    private View getViewById(int id){
        return activity.findViewById(id);
    }

    /**
     * This method returns number of admins in the login table
     * @return number of aadmins registered.
     */
    private int getNumberOfAdmins(){
        return beLoginDbOperations.getAllAdmins().size();
    }

    /**
     * This method is used to run on thread
     * @param runnable runnable which is to be run
     */
    public void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * This method is used to run the runnable on UI thread
     * @param runnable runnable which is to be run
     */
    public void runOnUiThread(Runnable runnable){
        activity.runOnUiThread(runnable);
    }


    /**
     * This method is called when the back button is pressed
     * @param object1 optional argument 1
     * @param object2 optional argument 2
     */
    @Override
    public void onBackPressed(Object object1, Object object2) {
        //Pop the back stack
       // boolean state=SRFragmentTransactionHelper.getInstance(activity).popBackStack();
        //Clear application variables
        activity.finish();
    }

    @Override
    public void onMenuSelected(int menuItemId, int menu) {

    }

    @Override
    public void onMoreMenuSelected() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Clear application variables
        BELoginMainActivity.fragmentBridge =null;
    }

    /**
     * This method is used to reset the user password
     */
    private void sendOtp(){
        otp = new SRSendMessageToPhone(activity).getRandomOtp(6);
        SRLog.getInstance().debug("OTP" + otp);
        String smsMessage = "Use " + otp
                + " as the OTP for completing your BillEzee  Password Reset. Valid for 15 minutes only. Please do not share your OTP with others."
                + "&rtype=json";

        String mailMessage = "Use " + otp
                + " as the OTP for completing your BillEzee Password Reset. Valid for 15 minutes only. Please do not share your OTP with others.";

        smsMessage = smsMessage.replaceAll("\\n", "%0A");
        smsMessage = smsMessage.replaceAll("\\s", "%20");
        smsMessage = smsMessage.replaceAll(":", "%3A");
        smsMessage = smsMessage.replaceAll("\\u20B9", "Rs");

        String url = "http://billpay.aapna-seva.in/api/sms.php?uid=" + SR_ACHAARIYA_UID + "&pin=" + SR_ACHAARIYA_PIN
                + "&sender=SSESDM&route=5&tempid=2&mobile=" + user.getUserPhone() + "&message=" + smsMessage + "&pushid=1";
        SRLog.getInstance().debug(url);
        progressDialog = new SRProgressDialog(activity);
        progressDialog.createProgressDialog(srGetResources.getTextFromResource(R.string.otp),
                srGetResources.getTextFromResource(R.string.pleasewait));
        SRSendMessageToPhone srSendMessageToPhone = new SRSendMessageToPhone(activity);
        srSendMessageToPhone.sendOtpThruAchaaria(url, BELoginMainPageFragment.this);
        //wait for sending otp call back

        //Also send OTP to mail
        SRSendMessageToMail srSendMessageToMail = new SRSendMessageToMail(activity);
        SRSendMailRequestObject srSendMailRequestObject = new SRSendMailRequestObject();
        srSendMailRequestObject.mailHost = SR_MAIL_HOST;
        srSendMailRequestObject.mailPort = SR_MAIL_PORT;
        srSendMailRequestObject.message = mailMessage;
        srSendMailRequestObject.senderMailId = SR_SRISHTI_MAIL_ID;
        srSendMailRequestObject.senderMailIdPassword = SR_SRISHTI_MAIL_PASSWORD;
        srSendMailRequestObject.receiverMailId = user.getUserEmailID();
        srSendMailRequestObject.tittle = "OTP for billeasy password reset";
        //send mail
        srSendMessageToMail.sendMail(srSendMailRequestObject);
    }


    /**
     * This method is called on completion of the countdown timer
     */
    @Override
    public void onCompletionTimer() {
        //on completion of the countdown timer close it
        srDisplayMessage.displayMessage(srGetResources.getTextFromResource(R.string.otpExpired),Color.RED);
        if(srAlertBuilder != null){
            srAlertBuilder.closeAlertDialog();
        }
    }

    @Override
    public void onResponceOtp(int errorCode) {
        getDelay(100);
        progressDialog.dismissProgressDialog();
        getDelay(100);
        if(errorCode==200){
            Runnable afterOtpSuccess = new Runnable() {
                @Override
                public void run() {
                    log.debug("Success OTP Sending");
                    //it gets verified
                    srAlertBuilder = new SRAlertBuilder(activity);
                    srAlertBuilder.createAlertBuilder();
                    View view = activity.getLayoutInflater().inflate(R.layout.otpentrypopup,null);
                    srAlertBuilder.setCustomView(view);
                    srAlertBuilder.showDialogBox();

                    //Get pop up custom view references
                    TextView timeout=(TextView) view.findViewById(R.id.T_OEP_messageLine3);
                    final EditText eotp = (EditText) view.findViewById(R.id.E_OEP_Otp);
                    Button back = (Button) view.findViewById(R.id.B_OEP_Back);
                    Button ok = (Button) view.findViewById(R.id.B_OEP_Ok);
                    Button resend =(Button) view.findViewById(R.id.B_OEP_Resend);

                    //Create the countdown timer
                    srCountDownTimer = new SRCountDownTimer(activity,SIGNUP_OTP_DURATION,timeout,Color.BLACK);
                    srCountDownTimer.setCallBackInterface(BELoginMainPageFragment.this);
                    srCountDownTimer.start_countdown_timer();

                    //Back button on click listeners
                    back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            srAlertBuilder.closeAlertDialog();
                        }
                    });
                    resend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(srAlertBuilder!=null){
                                srAlertBuilder.closeAlertDialog();
                            }
                            sendOtp();
                        }
                    });
                    //Ok button
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(eotp.getText().toString().equals("")){
                                eotp.setHint(srGetResources.getTextFromResource(R.string.pleaseEnterSentOtp));
                                eotp.setHintTextColor(Color.RED);
                                return;
                            }
                            srCountDownTimer.stop_countdown_timer();
                            if (otp.equals(eotp.getText().toString())) {
                                //Add user to DB
                                try {
                                    //Reset password to default password
                                    user.setUserPassword(RESETTED_PASSWORD);
                                    int ret = dbOperations.editUser(user);
                                    if(ret==0){
                                        srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.success), Color.GREEN);
                                    }else{
                                        srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.failure), Color.RED);
                                    }
                                }catch (Exception e){
                                    srDisplayMessage.displayMessage(e.getMessage(),Color.RED);
                                }
                                srAlertBuilder.closeAlertDialog();
                                srDisplayMessage.displayMessage(srGetResources.getTextFromResource(R.string.success),Color.GREEN);
                                //Go to main page after a delay
                                Handler login = new Handler(Looper.getMainLooper());
                                login.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //set data in application
                                        SRFrameworkApplication.SR_LOGIN_USERNAME=user.getUserID();
                                        SRFrameworkApplication.SR_LOGIN_USERROLE=user.getUserRole();
                                        SRFrameworkApplication.SR_LOGIN_USER_PHONE=user.getUserPhone();
                                        SRFrameworkApplication.SR_LOGIN_USER_MAIL=user.getUserEmailID();
                                        SRFrameworkApplication.SR_LOGIN_USER_PASSWORD=user.getUserPassword();
                                        //set data in shared preference
                                        SRSharedPrefrence srSharedPrefrence = SRSharedPrefrence.getInstance(activity);
                                        srSharedPrefrence.putString(USER_ID,SRFrameworkApplication.SR_LOGIN_USERNAME);
                                        srSharedPrefrence.putInt(USER_ROLE_KEY,SRFrameworkApplication.SR_LOGIN_USERROLE);
                                        //set user and role in drawer
                                        srActivityBridge.function3(SRFrameworkApplication.SR_LOGIN_USERNAME,getUserRoleFromCode(SRFrameworkApplication.SR_LOGIN_USERROLE));
                                        //Call the interface function in the BELoginMainActivity
                                        srActivityBridge.function2(null,null);
                                    }
                                },1000);
                            }else{
                                //OTP Do not match
                                srAlertBuilder.closeAlertDialog();
                                srDisplayMessage.displayMessage(srGetResources.getTextFromResource(R.string.otpDonotMatch),Color.RED);
                            }
                        }
                    });
                }
            };
            runOnUiThread(afterOtpSuccess);
        }else{
            log.debug("Failure error code"+errorCode);
            if(errorCode==-1){
                //No internet //error hitting server//network error
                srDisplayMessage.displayMessage(srGetResources.getTextFromResource(R.string.networkError),Color.RED);
            }else {
                //error reaching server
                srDisplayMessage.displayMessage(srGetResources.getTextFromResource(R.string.networkError)+errorCode,Color.RED);
            }
        }
    }

    /**
     * This method is used to introduce a delay in the current threda
     * @param delay delay in MS
     */
    private void getDelay(long delay){
        try{
            Thread.sleep(delay);
        }catch (Exception e){

        }
    }

    /**
     * This method is used to check the user and retrieve the data
     */
    private void checkUserAndRetriveData(){
        String userId= E_userId.getText().toString();
        if(userId.equals("")){
            srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.pleaseentervalidusername), Color.RED);
            return;
        }

        //load user by user id
        user = beLoginDbOperations.getUserByUserId(userId);
        if(user == null){
            srDisplayMessage.displayMessage(resources.getTextFromResource(R.string.userNotPresent), Color.RED);
            return;
        }else {
            if(user.getUserPhone().equals("") && user.getUserEmailID().equals("")){
                //phone not present
                String message =resources.getTextFromResource(R.string.phoneemail);
                message = message+" "+resources.getTextFromResource(R.string.notAvailable);
                srDisplayMessage.displayMessage(message, Color.RED);
                return;
            }

          //User present
            //Call reset password method
            //show alert dialog
            //Then reset
            final SRAlertBuilder alertBuilder = new SRAlertBuilder(activity);
            String resetAlert =resources.getTextFromResource(R.string.resetAlert);
            resetAlert = resetAlert+" "+RESETTED_PASSWORD+". "+resources.getTextFromResource(R.string.doyouwanttoReset);
            alertBuilder.createAlertBuilder(resources.getTextFromResource(R.string.reset_User),resetAlert
            );
            alertBuilder.addPositiveButton(resources.getTextFromResource(R.string.yes),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Yes reset it
                            Runnable resetPassword = new Runnable() {
                                @Override
                                public void run() {
                                    sendOtp();
                                }
                            };
                            runOnThread(resetPassword);
                        }
                    });

            alertBuilder.addNegetiveButton(resources.getTextFromResource(R.string.no),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                            alertBuilder.closeAlertDialog();
                        }
                    });
            alertBuilder.showDialogBox();
        }
    }

    @Override
    public void onStop() {
        new SRCloseKeyboard().closeTheKeyBoard(activity);
        super.onStop();
    }


}
