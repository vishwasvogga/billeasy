package com.srishti.be.loginoperations.dboperations;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.srishti.be.utility.SRLog;
import com.srishtiesdm.slogindb.SUserIdLibMain;
import com.srishtiesdm.slogindb.entity.User;
import com.srishtiesdm.slogindb.util.RandomPasswordGenerateor;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_ADMIN;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_ENCRYPTION_SECRET_KEY_LENGTH;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SHARED_PREFRENCE_KEY;

/**
 * This class provides methods to interact with the db
 */

public class BELoginDbOperations {
    private static BELoginDbOperations beLoginDbOperations=null;
    private Context context=null;
    private SUserIdLibMain dbInterface=null;

    /**
     * THis method is used to  get the singleton instance of this class
     * @param context context
     * @return singleton instance of BELoginDbOperations class
     */
    public static BELoginDbOperations getInstance(Context context){
        if(beLoginDbOperations==null){
            beLoginDbOperations = new BELoginDbOperations(context);
        }
        return beLoginDbOperations;
    }

    /**
     * Private constructor
     * @param context context
     */
    private BELoginDbOperations(Context context) {
        this.context = context;
        //Initialise Login DB
        dbInterface = new SUserIdLibMain(context);
        dbInterface.initializeUserDatabase();
    }

    /**
     * This method is used to add a user to the DB
     * @param user user object
     * @return 0-success , -1- failed, -2-duplicate record , -3 - db error
     */
    public int addUser(User user){
        getDelay(70);
        int ret = dbInterface.insertUserInformation(user);
        SRLog.getInstance().debug("adding user"+ret);
        if(ret==100){
            ret=0;
        }else if(ret==102){
            ret=-2;
        }else{
            ret=-1;
        }
        return ret;
    }

    /**
     * This method is used to edit the user
     * @param user user object
     * @return  0-success , -1- failed
     */
    public int editUser(User user){
        getDelay(70);
        boolean ret = dbInterface.updateUserData(user);
        SRLog.getInstance().debug("adding user"+ret);
       if(ret){
           return 0;
       }else{
           return -1;
       }
    }

    /**
     * This returns the user etails
     * @param userId id of the user , userid
     * @return user object
     */
    public User getUserByUserId(String userId){
        getDelay(100);
        int ret=-1;
        User user = dbInterface.getUserDataByUserId(userId);
        if(user==null){
            //no user present
            SRLog.getInstance().debug("user not present");
        }else{
            //user present
            SRLog.getInstance().debug("user  present");
        }
        return user;
    }

    /**
     * This returns the user etails
     * @param phonenumber Phone number of the user
     * @return user object
     */
    public User getUserByPhoneNumber(String phonenumber){
        getDelay(100);
        User user = dbInterface.getUserDataByPhoneNumber(phonenumber);
        if(user==null){
            //no user present
            SRLog.getInstance().debug("phonenumber not present");
        }else{
            //user present
            SRLog.getInstance().debug("phonenumber  present");
        }
        return user;
    }

    /**
     * This returns the user etails
     * @param mailId Mail ID of the user
     * @return user object
     */
    public User getUserByMailId(String mailId){
        getDelay(100);
        User user = dbInterface.getUserDataByEmailID(mailId);
        if(user==null){
            //no user present
            SRLog.getInstance().debug("mailId not present");
        }else{
            //user present
            SRLog.getInstance().debug("mailId  present");
        }
        return user;
    }

    /**
     * Delete a user from user ID
     * @param user user id of the string
     * @return retuns boolean
     */
    public boolean deleteUserByUserId(User user){
        return dbInterface.deleteUser(user);
    }

    /**
     * This will return all the users under that role
     * @param userRole 2-admin,3-super,4-operator
     * @return List<User>
     */
    public List<User> getAllUsersByUserrole(int userRole){
        return dbInterface.getUserDataByUserRole(userRole);
    }

    /**
     * This method is used to introduce a delay in the current threda
     * @param delay delay in MS
     */
    private void getDelay(long delay){
        try{
            Thread.sleep(delay);
        }catch (Exception e){

        }
    }

    /**
     * This method will return all the admins in the
     * Login database
     * @return
     */
    public List<User> getAllAdmins(){
        return dbInterface.getUserDataByUserRole(ROLE_ADMIN);
    }

    /**
     * This method genrates random secret key , which must be stored for entire application life
     * in shared preference for encryption and decryption of the password
     * @return secret key
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public SecretKey generateKey() throws Exception
    {
        byte[] key = (new RandomPasswordGenerateor().generateRandomStringPassword()).getBytes("UTF-8");
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // use only first 128 bit

        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
       // return   new SecretKeySpec(/*new RandomPasswordGenerateor().generateRandomStringPassword().getBytes()*/"12345678".getBytes(), "AES");
    }

    /**
     * This method is used to encrypt the password
     * @param message password to encrypt
     * @return encrypted password
     */
    public String encryptPassword(String message)
            throws Exception
    {
     /* Encrypt the message. */
        /*message=Base64.encodeToString(message.getBytes(),Base64.DEFAULT);
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, getEncryptionKeyFromSharedPreference());
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
        return  new String(cipherText);*/
        return message;
    }

    /**
     * This method is used to decrypt the password using secret key
     * @param cipherText encrypted password
     * @return password raw
     */
    public String decryptPassword(String cipherText)
            throws Exception
    {
    /* Decrypt the message, given derived encContentValues and initialization vector. */
       /* Cipher cipher = null;
        cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, getEncryptionKeyFromSharedPreference());
        String decryptString = new String(cipher.doFinal(cipherText.getBytes()), "UTF-8");
        return new String(Base64.decode(decryptString.getBytes(),Base64.DEFAULT));*/
       return cipherText;
    }

    /**
     * This method is used to get the encryption key from shared preference
     * @return Secret key
     */
    public SecretKey getEncryptionKeyFromSharedPreference() throws Exception{
        SharedPreferences sharedpreferences = ((Activity)context).
                getSharedPreferences(SR_SHARED_PREFRENCE_KEY, Context.MODE_PRIVATE);
        int encryptionSecretKey = sharedpreferences.getInt(SR_LOGIN_ENCRYPTION_SECRET_KEY_LENGTH,0);
        //To form key for each bytes
        String SECRET_KEY_STORE_BYTE_="secret_key_store_byte_";
        //check for secrete key present or not
        if(encryptionSecretKey==0){
            SharedPreferences.Editor editor = sharedpreferences.edit();
            // get bytes of secret key
            SecretKey secretKey = generateKey();
            byte[] secretKey_bytes =secretKey.getEncoded();
            int secret_key_length = secretKey_bytes.length;
            //Convert byte array to int array
            for(int i=0; i<secret_key_length;i++){
                SRLog.getInstance().debug("key byts"+secretKey_bytes[i]);
                String temp_key = SECRET_KEY_STORE_BYTE_+i;
                editor.putInt(temp_key,(int)secretKey_bytes[i]);
            }
            //Store in Shared preference
            editor.apply();
            editor.commit();
            SRLog.getInstance().debug("Login encryption Secret Key stored length: "+secret_key_length);
            return secretKey;
        }else{
            //Get int array length
            int byteArrayLengthFromSharedPref = sharedpreferences.getInt(SR_LOGIN_ENCRYPTION_SECRET_KEY_LENGTH,0);
            byte[] storedByteArray = new byte[byteArrayLengthFromSharedPref];

            //get the int array from shared preference and convert to byte array
            for(int i=0; i<byteArrayLengthFromSharedPref;i++){
                int stored_byte = sharedpreferences.getInt(SECRET_KEY_STORE_BYTE_+i,0);
                storedByteArray[i] = (byte) stored_byte;
                SRLog.getInstance().debug("byte"+ storedByteArray[i]);

            }
            // rebuild key using SecretKeySpec
            return new SecretKeySpec(storedByteArray, 0, byteArrayLengthFromSharedPref, "AES");
        }
    }
}
