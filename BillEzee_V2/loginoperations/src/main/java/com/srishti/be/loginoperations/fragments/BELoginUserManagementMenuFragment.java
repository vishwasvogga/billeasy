package com.srishti.be.loginoperations.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.srishti.be.loginoperations.R;
import com.srishti.be.utility.SRCloseKeyboard;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.activity.SRFrameworkApplication;
import com.srishti.be.utility.config.SRGetConfiguration;
import com.srishti.be.utility.interfaces.SRActivityBridge;

import static com.srishti.be.loginoperations.constants.BELoginConstants.USER_ROLE_KEY;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_DELETE;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_MODE;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_ONLY;
import static com.srishti.be.loginoperations.constants.BELoginConstants.VIEW_RESET;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_BUTTON_TINT_ALPHA;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_ADD_NEW_USER_FRAGMENT_TAG;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_ADD_VIEW_USERS_FRAGMENT_TAG;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_DELETE_FRAGMENT_TAG;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_LOGIN_RESET_FRAGMENT_TAG;

/**
 * This fragment gives the menu of user management
 */

public class BELoginUserManagementMenuFragment extends Fragment{
    private Activity activity=null;
    private LinearLayout baseLayout=null;
    private SRFragmentTransactionHelper srFragmentTransactionHelper=null;
    private SRActivityBridge srActivityBridge=null;
    private SRGetResources resources=null;
    private Button B_Add,B_View, B_Reset,B_Delete,B_Back=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        baseLayout=(LinearLayout) activity.getLayoutInflater().inflate(R.layout.usermanagement_layout,null);
        return baseLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        intialise();
        configure();
        addActionListeners();
    }

    /**
     * This method iniailises all the views and objects
     */
    private void intialise(){
        //Init UI
        B_Add = (Button) baseLayout.findViewById(R.id.B_UM_Add);
        B_View = (Button) baseLayout.findViewById(R.id.B_UM_View);
        B_Reset = (Button) baseLayout.findViewById(R.id.B_UM_ResetUser);
        B_Delete =(Button) baseLayout.findViewById(R.id.B_UM_Delete);
        B_Back =(Button) baseLayout.findViewById(R.id.B_UM_back);
        //Init class variables
        srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(activity);
        resources = SRGetResources.getInstance(activity);
    }

    /**
     * This method is used to configure the
     * configurables
     */
    private void configure(){
        srActivityBridge = (SRActivityBridge) activity;
        //set messageline
        srActivityBridge.function1(resources.getTextFromResource(R.string.manage_users),null);
        SRGetConfiguration srGetConfiguration = SRGetConfiguration.getInstance(activity);
        boolean isAddEnabled = srGetConfiguration.accessToSRCBELoginRegisterNewUserFragment;
        boolean isViewEnabled = srGetConfiguration.accessToSRCBELoginViewUsersFragment;
        boolean isResetEnabled = srGetConfiguration.accessToSRCBELoginResetUsersFlag;
        boolean isDeleteUserEnabled = srGetConfiguration.accessToSRCBELoginDeleteUsersFlag;
        if(!isAddEnabled){
            disabledAndTint(B_Add);
        }
        if(!isViewEnabled){
            disabledAndTint(B_View);
        }
        if(!isResetEnabled){
            disabledAndTint(B_Reset);
        }
        if(!isDeleteUserEnabled){
            disabledAndTint(B_Delete);
        }
    }

    /**
     * This method is used to add action listeners
     */
    private void addActionListeners(){
        B_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle configIn = new Bundle();
                configIn.putInt(USER_ROLE_KEY,SRFrameworkApplication.SR_LOGIN_USERROLE);
                BELoginRegisterNewUserFragment beLoginRegisterNewUserFragment = new BELoginRegisterNewUserFragment();
                beLoginRegisterNewUserFragment.setArguments(configIn);
                srFragmentTransactionHelper.ReplaceFragment(beLoginRegisterNewUserFragment,SR_LOGIN_ADD_NEW_USER_FRAGMENT_TAG,com.srishti.be.utility.R.id.mainFragmentContainer,SR_LOGIN_ADD_NEW_USER_FRAGMENT_TAG);
            }
        });

        B_View.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BELoginViewUsersFragment beLoginViewUsersFragment = new BELoginViewUsersFragment();
                Bundle configIn = new Bundle();
                configIn.putInt(VIEW_MODE,VIEW_ONLY);
                configIn.putInt(USER_ROLE_KEY, SRFrameworkApplication.SR_LOGIN_USERROLE);
                beLoginViewUsersFragment.setArguments(configIn);
                srFragmentTransactionHelper.ReplaceFragment(beLoginViewUsersFragment,SR_LOGIN_ADD_VIEW_USERS_FRAGMENT_TAG,com.srishti.be.utility.R.id.mainFragmentContainer,SR_LOGIN_ADD_VIEW_USERS_FRAGMENT_TAG);
            }
        });

        B_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });

        B_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BELoginViewUsersFragment beLoginViewUsersFragment = new BELoginViewUsersFragment();
                Bundle configIn = new Bundle();
                configIn.putInt(VIEW_MODE,VIEW_DELETE);
                configIn.putInt(USER_ROLE_KEY, SRFrameworkApplication.SR_LOGIN_USERROLE);
                beLoginViewUsersFragment.setArguments(configIn);
                srFragmentTransactionHelper.ReplaceFragment(beLoginViewUsersFragment,SR_LOGIN_DELETE_FRAGMENT_TAG,com.srishti.be.utility.R.id.mainFragmentContainer,SR_LOGIN_DELETE_FRAGMENT_TAG);
            }
        });

        B_Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BELoginViewUsersFragment beLoginViewUsersFragment = new BELoginViewUsersFragment();
                Bundle configIn = new Bundle();
                configIn.putInt(VIEW_MODE,VIEW_RESET);
                configIn.putInt(USER_ROLE_KEY, SRFrameworkApplication.SR_LOGIN_USERROLE);
                beLoginViewUsersFragment.setArguments(configIn);
                srFragmentTransactionHelper.ReplaceFragment(beLoginViewUsersFragment,SR_LOGIN_RESET_FRAGMENT_TAG,com.srishti.be.utility.R.id.mainFragmentContainer,SR_LOGIN_RESET_FRAGMENT_TAG);
            }
        });


    }

    @Override
    public void onStop() {
        new SRCloseKeyboard().closeTheKeyBoard(activity);
        super.onStop();
    }

    /**
     * This function is used to disable and tint the button
     * @param button button
     */
    private void disabledAndTint(Button button){
        button.setEnabled(false);
        button.setAlpha(SR_BUTTON_TINT_ALPHA);
    }
}
