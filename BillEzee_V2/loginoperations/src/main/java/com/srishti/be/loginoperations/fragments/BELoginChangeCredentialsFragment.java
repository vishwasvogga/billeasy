package com.srishti.be.loginoperations.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srishti.be.loginoperations.R;
import com.srishti.be.loginoperations.dboperations.BELoginDbOperations;
import com.srishti.be.utility.SRAlertBuilder;
import com.srishti.be.utility.SRCloseKeyboard;
import com.srishti.be.utility.SRCountDownTimer;
import com.srishti.be.utility.SRDisplayMessage;
import com.srishti.be.utility.SRFragmentTransactionHelper;
import com.srishti.be.utility.SRGetResources;
import com.srishti.be.utility.SRLog;
import com.srishti.be.utility.SRProgressDialog;
import com.srishti.be.utility.activity.SRFrameworkApplication;
import com.srishti.be.utility.interfaces.SRActivityBridge;
import com.srishti.be.utility.mail.SRSendMailRequestObject;
import com.srishti.be.utility.mail.SRSendMessageToMail;
import com.srishti.be.utility.phone.SRSendMessageToPhone;
import com.srishtiesdm.slogindb.entity.User;

import static android.view.View.GONE;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_ADMIN;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_OPERATOR;
import static com.srishti.be.loginoperations.constants.BELoginConstants.ROLE_SUPERVISOR;
import static com.srishti.be.loginoperations.constants.BELoginConstants.SIGNUP_OTP_DURATION;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_PIN;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_ACHAARIYA_UID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_HOST;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_MAIL_PORT;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_ID;
import static com.srishti.be.utility.constants.SRApplicationConstants.SR_SRISHTI_MAIL_PASSWORD;

/**
 * This fragment is used to change the users credentials
 */

public class BELoginChangeCredentialsFragment extends Fragment implements  SRSendMessageToPhone.onOtpSendingResponce,
        SRCountDownTimer.onTimerEvents{
    private LinearLayout baseLayout = null;
    private Button B_Edit,B_Back=null;
    private EditText E_UserId,E_PhoneNumber,E_MailId,E_Password,E_ConfirmPassword=null;
    private TextInputLayout TIL_PhoneNUmber,TIL_MailId=null;
    private TextView T_MessageLine=null;
    private Activity activity=null;
    private SRFragmentTransactionHelper srFragmentTransactionHelper=null;
    private SRGetResources srGetResources=null;
    private BELoginDbOperations dbOperations=null;
    private SRDisplayMessage displayMessage=null;
    private SRLog log=null;
    private User OldDetailsOfUser=null;
    private SRActivityBridge srActivityBridge=null;

    private SRProgressDialog progressDialog=null;
    private SRAlertBuilder srAlertBuilder=null;
    private SRCountDownTimer srCountDownTimer=null;
    private String otp="";
    private String phoneNumber,mailId,password="";
    int userRoleInt = 4;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = (LinearLayout) inflater.inflate(R.layout.registernewuser_layout,null);
        baseLayout =(LinearLayout) view;
        activity = getActivity();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initialise();
        configure();
        onActionListener();
    }

    /**
     * Thismethod i s used to initilise various components
     */
    private void initialise(){
        B_Edit=(Button) baseLayout.findViewById(R.id.B_RNU_Add);
        B_Back=(Button) baseLayout.findViewById(R.id.B_RNU_Back);
        E_UserId=(EditText) baseLayout.findViewById(R.id.E_RNU_userId);
        E_MailId=(EditText) baseLayout.findViewById(R.id.E_RNU_mailId);
        E_PhoneNumber=(EditText) baseLayout.findViewById(R.id.E_RNU_PhoneNumber);
        E_Password=(EditText) baseLayout.findViewById(R.id.E_RNU_password);
        E_ConfirmPassword=(EditText) baseLayout.findViewById(R.id.E_RNU_confirmPassword);
        srFragmentTransactionHelper = SRFragmentTransactionHelper.getInstance(activity);
        T_MessageLine=(TextView) baseLayout.findViewById(R.id.T_RNU_messageLine);
        TIL_PhoneNUmber = (TextInputLayout) baseLayout.findViewById(R.id.TI_RNU_phoneNumber);
        TIL_MailId=(TextInputLayout) baseLayout.findViewById(R.id.TI_RNU_mailId);
        srGetResources = SRGetResources.getInstance(activity);
        srFragmentTransactionHelper= SRFragmentTransactionHelper.getInstance(activity);
        dbOperations = BELoginDbOperations.getInstance(activity);
        displayMessage = new SRDisplayMessage(activity);
        displayMessage.configure(true,3000,300,false,T_MessageLine);
        log = SRLog.getInstance();
        userRoleInt = 4;
    }

    /**
     * This method is used to configure various configarable
     */
    private void configure(){
        //get user role
        userRoleInt = SRFrameworkApplication.SR_LOGIN_USERROLE;
        //get activity bridge interface
        srActivityBridge =(SRActivityBridge) activity;
        //Hide radio buttons
        LinearLayout userRole = (LinearLayout) baseLayout.findViewById(R.id.LL_RNU_userRole);
        userRole.setVisibility(GONE);
        //Hide User role text view
        TextView userRoleText=(TextView) baseLayout.findViewById(R.id.T_RNU_userRole);
        userRoleText.setVisibility(GONE);
        //Change button add to change
        B_Edit.setText(srGetResources.getTextFromResource(R.string.change));
        //Change message line to change credentials
        T_MessageLine.setText(srGetResources.getTextFromResource(R.string.Edit_credentials));
        //Disable USER ID entry
        E_UserId.setEnabled(false);
        //set UI from user role
        if(userRoleInt == ROLE_ADMIN || userRoleInt == 1){
            //Do nothing
        }else if (userRoleInt==ROLE_SUPERVISOR || userRoleInt==ROLE_OPERATOR){
            //Set optional text
            String EEmailIdText = TIL_MailId.getHint().toString();
            EEmailIdText = EEmailIdText+" "+srGetResources.getTextFromResource(R.string.optional);
            TIL_MailId.setHint(EEmailIdText);

            String EPhonenumber = TIL_PhoneNUmber.getHint().toString();
            EPhonenumber = EPhonenumber+" "+srGetResources.getTextFromResource(R.string.optional);
            TIL_PhoneNUmber.setHint(EPhonenumber);
        }else{
            //Set optional text
            String EEmailIdText = TIL_MailId.getHint().toString();
            EEmailIdText = EEmailIdText+" "+srGetResources.getTextFromResource(R.string.optional);
            TIL_MailId.setHint(EEmailIdText);

            String EPhonenumber = TIL_PhoneNUmber.getHint().toString();
            EPhonenumber = EPhonenumber+" "+srGetResources.getTextFromResource(R.string.optional);
            TIL_PhoneNUmber.setHint(EPhonenumber);
        }
        //Get the existsing details from DB , from userID
        getTheExistingUserDetailsFromDb();
    }

    /**
     * This method adds action listeners to the various UI elements
     */
    private void onActionListener(){
        B_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srFragmentTransactionHelper.popBackStack();
            }
        });

        B_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Runnable fieldsCheck = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            fieldsCheck();
                        }catch (Exception e){
                            displayMessage.displayMessage(e.getMessage(),Color.RED);
                            e.printStackTrace();
                        }
                    }
                };
                runOnThread(fieldsCheck);
            }
        });
    }

    /**
     * This function fills the UI with exisiting user details from the DB
     */
    private void getTheExistingUserDetailsFromDb(){
        Runnable getExistingUserDetails = new Runnable() {
            @Override
            public void run() {
                final User user=OldDetailsOfUser=dbOperations.getUserByUserId(SRFrameworkApplication.SR_LOGIN_USERNAME);
                Runnable setUI = new Runnable() {
                    @Override
                    public void run() {
                        E_UserId.setText(user.getUserID());
                        E_MailId.setText(user.getUserEmailID());
                        E_PhoneNumber.setText(user.getUserPhone());
                    }
                };
                runOnUiThread(setUI);
            }
        };
        runOnThread(getExistingUserDetails);
    }

    /**
     * This method is used to run on thread
     * @param runnable runnable which is to be run
     */
    public void runOnThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * This method is used to run the runnable on UI thread
     * @param runnable runnable which is to be run
     */
    public void runOnUiThread(Runnable runnable){
        activity.runOnUiThread(runnable);
    }

    /**
     * This method is used to validate all the fields
     */
    private void fieldsCheck() throws Exception{
        //Check user ID
        String userId= E_UserId.getText().toString();
        if(userId.equalsIgnoreCase("")){
            displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.pleaseentervalidusername), Color.RED);
            return;
        }else{
            if(OldDetailsOfUser.getUserID().equals(userId)){
                //old user id // no new user id enterd
            }else {
                //Check for that user name in DB
                User user = dbOperations.getUserByUserId(userId);
                if (user == null) {
                    //no user present proceed
                } else {
                    displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.useridalreadypresent), Color.RED);
                    log.debug("User already present " + user.toString());
                    return;
                }
            }
        }
        String mobileNumber = this.phoneNumber = E_PhoneNumber.getText().toString();
        String mailId = this.mailId = E_MailId.getText().toString();
        //Phone end email check only for the admin
        if(userRoleInt==ROLE_ADMIN ||userRoleInt==1  ) {
            //Check Mobile number

            if (mobileNumber.equalsIgnoreCase("") || (mobileNumber.length() != 10)) {
                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.pleaseentervalidphonenumber), Color.RED);
                return;
                //Below is the code to prevent duplicate mobile number
            }/*else{
            if(OldDetailsOfUser.getUserPhone().equals(mobileNumber)){
                //Do nothing , no new phone enetred
            }else {
                //Check for that user name in DB
                User user = dbOperations.getUserByPhoneNumber(mobileNumber);
                if (user == null) {
                    //no user present proceed
                } else {
                    displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.phonenumberalreadypresent), Color.RED);
                    return;
                }
            }
        }*/

            //Check Mail id

            if (mailId.equalsIgnoreCase("") || !(mailId.contains("@"))) {
                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.pleaseentervalidemailid), Color.RED);
                return;
                //Below is the code to prevent duplicate mal ID
            }/*else{
            if(OldDetailsOfUser.getUserEmailID().equals(mailId)){
                //Do nothing , email has ot edited
            }else {
                //Check for that user name in DB
                User user = dbOperations.getUserByMailId(mailId);
                if (user == null) {
                    //no user present proceed
                } else {
                    displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.mailidalreadypresent), Color.RED);
                    return;
                }
            }
        }*/
        }

        //Check pasword field
        String pasword= this.password=E_Password.getText().toString();
        if(pasword.equalsIgnoreCase("") ){
            displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.pleaseentervalidpassword), Color.RED);
            return;
        }else {
            //check for confirm password
            String cpasword = E_ConfirmPassword.getText().toString();
            if (cpasword.equalsIgnoreCase("")) {
                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.pleaseconfirmpassword), Color.RED);
                return;
            } else if (!cpasword.equalsIgnoreCase(pasword)) {
                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.passwordsdonotmatch), Color.RED);
                return;
            } else {
                //Password confirmed
                //check the user
                //if admin send OTP and wait from iit
                if(SRFrameworkApplication.SR_LOGIN_USERROLE==ROLE_ADMIN){
                    sendOtp();

                }else{//supervisor/operator proceed without OTP
                    editUser(phoneNumber,mailId,pasword);
                }
            }
    }}

    /**
     * this method is used to pop back stack after a delay
     * @param delay
     */
    private void popBackStackWithDelay(final long delay){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(delay);
                   srFragmentTransactionHelper.popBackStack();
                }catch (Exception e){

                }
            }
        }).start();
    }

    /**
     * THis method returns the user role string from user role int
     * @param userrole integer code
     * @return role in string
     */
    private String getUserRoleFromCode(int userrole){
        switch (userrole){
            case 1: return srGetResources.getTextFromResource(R.string.super_admin);
            case 2: return srGetResources.getTextFromResource(R.string.admin);
            case 3: return srGetResources.getTextFromResource(R.string.supervisor);
            case 4: return srGetResources.getTextFromResource(R.string.operator);
        }
        return "";
    }

    /**
     * This module edits the user
     */
    private void editUser(String mobileNumber,String mailId,String pasword) throws Exception {
        User user = OldDetailsOfUser;
        user.setUserPhone(mobileNumber);
        user.setUserEmailID(mailId);
        user.setUserPassword(dbOperations.encryptPassword(pasword));

        log.debug("New Password = " + user.getUserPassword());
        int ret = dbOperations.editUser(user);
        if (ret == 0) {
            //Adding user success
            displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.success), Color.GREEN);
            //set data in application
            SRFrameworkApplication.SR_LOGIN_USERNAME = user.getUserID();
            SRFrameworkApplication.SR_LOGIN_USERROLE = user.getUserRole();
            SRFrameworkApplication.SR_LOGIN_USER_PHONE = user.getUserPhone();
            SRFrameworkApplication.SR_LOGIN_USER_MAIL = user.getUserEmailID();
            SRFrameworkApplication.SR_LOGIN_USER_PASSWORD=user.getUserPassword();
            popBackStackWithDelay(1500);
        } else {
            displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.failure), Color.RED);
        }
    }



        /**
         * This method is called on completion of the countdown timer
         */
        @Override
        public void onCompletionTimer() {
            //on completion of the countdown timer close it
            displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.otpExpired),Color.RED);
            if(srAlertBuilder != null){
                srAlertBuilder.closeAlertDialog();
            }
        }

    @Override
    public void onResponceOtp(int errorCode) {
        getDelay(100);
        progressDialog.dismissProgressDialog();
        getDelay(100);
        if(errorCode==200){
            Runnable afterOtpSuccess = new Runnable() {
                @Override
                public void run() {
                    log.debug("Success OTP Sending");
                    //it gets verified
                    srAlertBuilder = new SRAlertBuilder(activity);
                    srAlertBuilder.createAlertBuilder();
                    View view = activity.getLayoutInflater().inflate(R.layout.otpentrypopup,null);
                    srAlertBuilder.setCustomView(view);
                    srAlertBuilder.showDialogBox();

                    //Get pop up custom view references
                    TextView timeout=(TextView) view.findViewById(R.id.T_OEP_messageLine3);
                    final EditText eotp = (EditText) view.findViewById(R.id.E_OEP_Otp);
                    Button back = (Button) view.findViewById(R.id.B_OEP_Back);
                    Button ok = (Button) view.findViewById(R.id.B_OEP_Ok);
                    Button resend =(Button) view.findViewById(R.id.B_OEP_Resend);

                    //Create the countdown timer
                    srCountDownTimer = new SRCountDownTimer(activity,SIGNUP_OTP_DURATION,timeout,Color.BLACK);
                    srCountDownTimer.setCallBackInterface(BELoginChangeCredentialsFragment.this);
                    srCountDownTimer.start_countdown_timer();

                    //Back button on click listeners
                    back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(srAlertBuilder!=null) {
                                srAlertBuilder.closeAlertDialog();
                            }
                        }
                    });

                    resend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(srAlertBuilder!=null){
                                srAlertBuilder.closeAlertDialog();
                            }
                            sendOtp();
                        }
                    });
                    //Ok button
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(eotp.getText().toString().equals("")){
                                eotp.setHint(srGetResources.getTextFromResource(R.string.pleaseEnterSentOtp));
                                eotp.setHintTextColor(Color.RED);
                                return;
                            }
                            srCountDownTimer.stop_countdown_timer();
                            if (otp.equals(eotp.getText().toString())) {
                                //Add user to DB
                                try {
                                    editUser(phoneNumber, mailId, password);
                                }catch (Exception e){
                                    displayMessage.displayMessage(e.getMessage(),Color.RED);
                                }
                                srAlertBuilder.closeAlertDialog();
                                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.success),Color.GREEN);
                                popBackStackWithDelay(1000);
                            }else{
                                //OTP Do not match
                                srAlertBuilder.closeAlertDialog();
                                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.otpDonotMatch),Color.RED);
                            }
                        }
                    });
                }
            };
            runOnUiThread(afterOtpSuccess);
        }else{
            log.debug("Failure error code"+errorCode);
            if(errorCode==-1){
                //No internet //error hitting server//network error
                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.networkError),Color.RED);
            }else {
                //error reaching server
                displayMessage.displayMessage(srGetResources.getTextFromResource(R.string.networkError)+errorCode,Color.RED);
            }
        }
    }

    /**
     * This method is used to introduce a delay in the current threda
     * @param delay delay in MS
     */
    private void getDelay(long delay){
        try{
            Thread.sleep(delay);
        }catch (Exception e){

        }
    }

    @Override
    public void onStop() {
        new SRCloseKeyboard().closeTheKeyBoard(activity);
        super.onStop();
    }

    /**
     * this method is used to send the OTP
     */
    private void sendOtp(){
        otp = new SRSendMessageToPhone(activity).getRandomOtp(6);
        SRLog.getInstance().debug("OTP" + otp);
        String smsMessage = "Use " + otp
                + " as the OTP for completing your BillEzee  Password Reset. Valid for 15 minutes only. Please do not share your OTP with others."
                + "&rtype=json";

        String mailMessage = "Use " + otp
                + " as the OTP for completing your BillEzee Password Reset. Valid for 15 minutes only. Please do not share your OTP with others.";

        smsMessage = smsMessage.replaceAll("\\n", "%0A");
        smsMessage = smsMessage.replaceAll("\\s", "%20");
        smsMessage = smsMessage.replaceAll(":", "%3A");
        smsMessage = smsMessage.replaceAll("\\u20B9", "Rs");

        String url = "http://billpay.aapna-seva.in/api/sms.php?uid=" + SR_ACHAARIYA_UID + "&pin=" + SR_ACHAARIYA_PIN
                + "&sender=SSESDM&route=5&tempid=2&mobile=" + this.phoneNumber + "&message=" + smsMessage + "&pushid=1";
        log.debug(url);
        progressDialog = new SRProgressDialog(activity);
        progressDialog.createProgressDialog(srGetResources.getTextFromResource(R.string.otp),
                srGetResources.getTextFromResource(R.string.pleasewait));
        SRSendMessageToPhone srSendMessageToPhone = new SRSendMessageToPhone(activity);
        srSendMessageToPhone.sendOtpThruAchaaria(url, this);
        //wait for sending otp call back

        //Also send OTP to mail
        SRSendMessageToMail srSendMessageToMail = new SRSendMessageToMail(activity);
        SRSendMailRequestObject srSendMailRequestObject = new SRSendMailRequestObject();
        srSendMailRequestObject.mailHost = SR_MAIL_HOST;
        srSendMailRequestObject.mailPort = SR_MAIL_PORT;
        srSendMailRequestObject.message = mailMessage;
        srSendMailRequestObject.senderMailId = SR_SRISHTI_MAIL_ID;
        srSendMailRequestObject.senderMailIdPassword = SR_SRISHTI_MAIL_PASSWORD;
        srSendMailRequestObject.receiverMailId = mailId;
        srSendMailRequestObject.tittle = "OTP for billeasy password reset";
        //send mail
        srSendMessageToMail.sendMail(srSendMailRequestObject);
    }

}
